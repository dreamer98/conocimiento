export function decorateToast(Ctrl) {
	[
		'Success',
		'Error',
		'Alert',
		'Info'
	].forEach(method => {
		Ctrl.prototype['show' + method] = function (message) {
			let toast = this.create({
				message,
				duration: 4000,
				position: "bottom"
			})
			return toast.present(toast)
		}
	})
}