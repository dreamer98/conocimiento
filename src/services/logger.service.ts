import env from '../env'

import { Injectable } from '@angular/core'

const global = window as any

@Injectable()
export class LoggerService {
  logEngine = global.console

  static LOG_LEVELS = {
    log: 3,
    warn: 2,
    error: 1
  }

  private _prefixMessage(logType: string, message?: string) {
    return `[${logType.toUpperCase()} ${new Date().toTimeString().slice(0, 8)}] ${message}`
  }

  private _log(logType: string, message?: string, ...args: any[]) {
    if (LoggerService.LOG_LEVELS[logType] >= (global.__LOG_LEVEL__ || env.LOG_LEVEL)) {
      this.logEngine[logType](this._prefixMessage(logType, message), ...args)
    }
  }

  group(label: string, fn: Function) {
    console.group(this._prefixMessage('group', label))

    // Pass a plain logger
    fn(console.log.bind(console))

    console.groupEnd()
  }

  log(...args) {
    this._log('log', ...args)
  }

  warn(...args) {
    this._log('warn', ...args)
  }

  error(...args) {
    this._log('error', ...args)
  }
}
