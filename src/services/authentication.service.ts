import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'




@Injectable()
export class AuthenticationService {

  valor:Array<any> = [ ]

  constructor(
    private _http: HttpClient
  ) {}
  login(): Promise<any> {
    return Promise.resolve()
    //return this._http.post(AuthenticationService.RESOURCE + '/login', user).toPromise()
  }


  logout(): Promise<any> {
    return this._http.post('@backend/usuarios/logout',{}).toPromise()
  }

  ingresar (user): Promise<any>{
    return this._http.post ('usuario/public',{
    username: user.numero,
    password: user.password
    }).toPromise()
  }
  register (user,tipoDocumento): Promise<any>{//cambiar en el momento que se requiera por la interfaces del responds

    return this._http.post ( '/beneficiarios',{
    tipoDocumento: tipoDocumento,
    numeroDocumento:user.numero,
    correoElectronico:user.correo,
    telefonoMovil:user.telefono,
    empleador:{
    tipoDocumento:user.tipoDocumento,
    numeroDocumento:user.numeroDocumentoRazonSocial,
  },
  aceptaTerminos: "true",
  } ).toPromise()
  }
  validate (tipodocumento , numerodocumento): Promise<any>{//cambiar en el momento que se requiera por la interfaces del responds
return this._http.get ( '/beneficiarios/'+tipodocumento+'/'+numerodocumento).toPromise()

}

activacionCuenta (documento , clave , codigo):Promise<any>{
    return this._http.post('/usuarios/activeAccount',{
    username: documento.numero,
    activationCode:  clave,
    password : codigo,
  }).toPromise()
}

restaurarContraseña(tipoDocumento , documento):Promise<any>{
 console.log(tipoDocumento)
 console.log(documento)
   return this._http.post('@backend/usuarios/reset',{
    tipoDocumento:tipoDocumento,
    numeroDocumento:documento
  }).toPromise()
}
autenticacion(user/*, info*/):Promise<any>{
  return this._http.post('/usuarios/login',{
    username:user.numero,
    password:user.password/*,
    sesionUsuario: info*/
  }).toPromise()
}

}
