
import { Injectable } from '@angular/core'
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoadingService {

 private loading:any=null;
 private timeOut:any=null;
  constructor(
    private loadCtrl:LoadingController
  ) {
  }

  Normal(message?:string,secondsToDismiss?:number,callback?:Function){
      let content="";
      this.DismissTimeOut();
      if(message!==undefined){
        content=message;
      }
      
    if(this.loading==null){
      this.loading =  this.loadCtrl.create({
        spinner: 'crescent',
        content: content,
        cssClass: 'positiva-loading',
        dismissOnPageChange: false
      })
    this.loading.present();
    this.loading.onDidDismiss(()=>{
      this.loading=null;
      this.DismissTimeOut();
    });
    }else {
     this.loading.data.content=content;
    }
      if(secondsToDismiss!==undefined && secondsToDismiss>0){
      this.timeOut=setTimeout(()=>{
        this.DismissLoading();
        if(callback!==undefined){
          callback();
        }
      },secondsToDismiss*1000);
    }
  }

  Hide(secondsToDismiss?:number,callback?:Function){
    this.DismissTimeOut();
    if(this.loading!==null && secondsToDismiss===undefined){
        this.DismissLoading();  
    }else if(this.loading!==null && secondsToDismiss!==undefined){
      this.timeOut=setTimeout(()=>{
        this.DismissLoading();
        if(callback!==undefined){
          callback();
        }
      },secondsToDismiss*1000);
    }
  }

  private DismissLoading(){
    let that=this;
    try{
      if(that.loading!==null && !that.loading._detached){
        that.loading.dismiss();
      }
      
    }catch(e){
      console.log(e);
    }
  }

  private DismissTimeOut(){
    if(this.timeOut!==null){
      clearTimeout(this.timeOut);
      this.timeOut=null;
    }
  }
  

}
