// Vendor
import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'




@Injectable()
export class ItemsService {

  constructor(
    private _http: HttpClient
  ) { }

  getCertificate(userInfo): Promise<any> {
    return this._http.post('@beneficiario/certificates_formats/generate', {
      certificate: {
        id_c: 1,
        user: {
          names: userInfo.primerNombre + ' ' + userInfo.segundoNombre,
          lastnames: userInfo.primerApellido + ' ' + userInfo.segundoApellido,
          document_type: "CC",
          document: userInfo.numeroDocumento,
        }
      }
    }).toPromise()
  }

  /*
  BACKEND NUEVO
  */
  // Servicio Carnet
  getVersionMin(): Promise<any> {
    return this._http.get('@backend/app/version').toPromise()
  }

  datosCarnet(): Promise<any> {
    return this._http.get('@backend/carnet').toPromise()
  }

  departamentos(): Promise<any> {
    return this._http.get("@backend/punto-atencion/departamento").toPromise()
  }

  municipios(departamento): Promise<any> {
    return this._http.get("@backend/punto-atencion/departamento/" + departamento + "/ciudad").toPromise()
  }

  prodecimiento():Promise<any>{
    return this._http.get("@backend/punto-atencion/prodecimiento").toPromise();
  }

  vistaPuntosAtencion(codigoDepartamento, codigoProceso): Promise<any> {
    return this._http.get("@backend/punto-atencion/"+codigoDepartamento+"/"+codigoProceso).toPromise()
  }

  incapacidadesPermanentes(usuario): Promise<any> {
    return this._http.get("@backend/incapacidades/permanentes").toPromise()
  }

  incapacidadesTemporales(usuario): Promise<any> {
    return this._http.get('@backend/incapacidades/temporales').toPromise()
  }

  enviarPqrd(mensaje): Promise<any> {
    return this._http.post("@backend/pqrs", {
       id:"",
       comentario:mensaje,
       fechaCreacion:""
    }).toPromise()
  }

  citasRehabilitacion():Promise<any> {
    return this._http.get("@backend/citas").toPromise()
  }
cancelarCita(id):Promise<any>{
  return  this._http.post("@backend/citas/cancelar",{
    id:+id,
  }).toPromise()
}

confirmarCita(id):Promise<any>{
  return this._http.post("@backend/citas/confirmar",{
    id:+id,
  }).toPromise()
}

redAsistencia(position,radioBuscado=10 ): Promise<any> {
  return this._http.get(`@backend/sedes/${position.latitude}/${position.longitude}/${radioBuscado}`).toPromise()
   }

   noticias(): Promise<any> {
    return this._http.get('@backend/noticias').toPromise()
  }

  telefonos(phoneCode:string): Promise<any> {
    return this._http.get(`@backend/telefono/${phoneCode}`).toPromise()
  }


  /*
 * BACKEND VIEJO AÚN  SE UTILIZA
 */
  informacionDeInteres(): Promise<any> {
    return this._http.get(`@beneficiario/news`).toPromise()
  }






  telefonosRamas(telefono): Promise<any> {
    return this._http.get("@beneficiario/phone-numbers?segment=" + encodeURIComponent(telefono).replace(/%20/g, '+')).toPromise()
  }

  /*
 * BACKEND VIEJO YA NO SE UTILIZA
 */


carnet(): Promise<any> {
  return this._http.get('@beneficiario/carnets_formats').toPromise()
}

getPhoneNumbers(segment): Promise<any> {
  return this._http.get('@beneficiario/phone-numbers?segment=' + encodeURI(segment)).toPromise()
}

puntosAtencion(): Promise<any> {
  return this._http.get("@beneficiario/procedures?orderBy=id&orderDirection=ASC").toPromise()
}

asistencia(cedula,position, codigoDepartamento=11 , codigoMunicipio=1 ,radioBuscado=10 ): Promise<any> {
  return this._http.get(`@beneficiario/assistance-network/providers?
  city_code=001&
  department_code=11&
  document_number=${cedula}&
  document_type=CC&
  latitude=${position.latitude}
  &longitude=${position.longitude}`).toPromise()
 // return this._http.get(`@backend/sedes/${position.latitude}/${position.longitude}/${codigoDepartamento}/${codigoMunicipio}/${radioBuscado}`).toPromise()
}


}
