
import { Injectable } from '@angular/core'
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastService {

  position={
    TOP:'top',
    BOTTOM:'bottom',
    MIDDLE:'middle'
  }
  constructor(
    private toastCtrl:ToastController
  ) {
   
  }

  ShowMessage(message:string,durationSeconds?:number,position?:string,callback?:Function){
    let toastPosition='bottom';
    let toastDuration=5;
    if(position!==undefined){
      toastPosition=position;
    }
    if(durationSeconds!==undefined){
      toastDuration=durationSeconds;
    }
    
    let toast = this.toastCtrl.create({
      message: message,
      duration: toastDuration*1000,
      position: toastPosition,
   //   dismissOnPageChange:true,
      cssClass:"custom-toast"
    });
  
    toast.onDidDismiss(() => {
      if(callback!==undefined){
              callback();
      }
    });
  
    toast.present();
  }

  ShowError(message:string,position?:string,callback?:Function){
    let toastPosition='bottom';
    if(position!==undefined){
      toastPosition=position;
    }
 
    
    let toast = this.toastCtrl.create({
      message: message,
     
      position: toastPosition,
      showCloseButton:true,
      closeButtonText:"OK",
     // dismissOnPageChange:true,
      cssClass:"custom-toast"
    });
  
    toast.onDidDismiss(() => {
      if(callback!==undefined){
              callback();
      }
    });
  
    toast.present();
  }
  
  

}
