import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { StoreService } from './store.service'


@Injectable()
export class ConsultService {

  // TODO: Externalize map
  // TODO: See other document types
  static DOCUMENT_TYPE_MAP = {
    CEDULA_DE_CIUDADANIA: 'CC'
  }

  constructor(
    private $http: HttpClient,
    private $store: StoreService
  ) {}

  /**
   * Append recursively data to the form
   *
   * @param {FormData} form
   * @param {string} key
   * @param {*} value
   *
   * @return void
   */
  static appendFormData(form: FormData, key: string, value: any) {
    if (typeof value !== 'object') {
      return form.append(key, value)
    }

    Object.keys(value).forEach(_key => {
      ConsultService.appendFormData(form, `${key}[${_key}]`, value[_key])
    })
  }

  /**
   * Get a fresh user
   *
   * @type {Object}
   */
  get user() {
    console.log(this.$store.getParse('user'));
    return this.$store.getParse('user')
  }

  getSinisters () {
    return this.$http.get('/siniestros').toPromise()
  }

  /*
  getSinisters(forceRequest): Promise<any> {
    let client: Client

    return this.$http.get('http://192.168.140.21:9988/ws/comprobacion/derechos?wsdl', { responseType: 'text' })
      .toPromise()
      .then(response => {
        client = this.$soap.createClient(response)

        return client.operation('buscarTrabajador', {
          numeroDocumento: '84026737',
          tipoDocumento: 'CC'
        })
      })
      .then(operation => {
        if (operation.error) {
          console.log('OPERATION ERROR:', operation.error)
          return
        }

        return this.$http.post(operation.url, operation.xml, { headers: operation.headers, responseType: 'text' }).toPromise()
      })
      .then(response => {
        const json = client.parseResponseBody(response)

        console.log(json)
      })
      .catch(error => {
        console.log('SOAP ERROR:', error)
      })
  }*/

  getRequests() {
    return this.$http.get('/solicitudes').toPromise()
  }

  sendAttachments(data, files: File[]): Promise<any> {
    // TODO: check https://angular.io/api/forms/FormGroup
    const form = new FormData()

    files.forEach(file => {
      form.append('files', file)
    })

    form.append('solicitud', JSON.stringify(data))

    // ConsultService.appendFormData(form, 'solicitud', data)

    return this.$http.post('/solicitudes', form).toPromise()
  }

  evaluate(data): Promise<any> {
    return this.$http.post('@beneficiario/assistance-network/evaluate', data).toPromise()
  }
}
