
import { Injectable } from '@angular/core'
import 'rxjs';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
declare let google;
@Injectable()
export class GoogleMapsProvider {
  map: any;
  coordenates: Geoposition;
  divMap:any;
  markers:any;
  googleMapsEvents:any;
  markersID:any={
    USER: 1,
    ATENCION:2,
    URGENCIAS:3
  };
  Icons:any;
  mapOptions:any;
  bounds:any;




  constructor(
      private geolocation:Geolocation
    ) {

        console.log('Hello GoogleMapsProvider Provider');


    this.divMap={};
    this.markers = [];
    this.googleMapsEvents={
      idle:null,
      dragEnd:null
    };





    this.mapOptions = {

      zoom: 16,

      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false,
      styles:[
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.attraction",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi.government",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "poi.place_of_worship",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi.school",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi.sports_complex",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#dadada"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#c9c9c9"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }
      ]

    };
    }

    static NumberCultureInfo(Num) {
        return Number(Num.toString().replace(",","."));
      }

      newCenter(lat,lng,callback){

        let center = new google.maps.LatLng(GoogleMapsProvider.NumberCultureInfo(lat),GoogleMapsProvider.NumberCultureInfo(lng));
        callback(center);

      }
      PushMarkerUserInMap (latitud,longitud) {
        let marker = new google.maps.Marker({
          position: new google.maps.LatLng(GoogleMapsProvider.NumberCultureInfo(latitud),GoogleMapsProvider.NumberCultureInfo(longitud)),
          map: this.map,
         // icon: 'http://maps.google.com/mapfiles/ms/micons/man.png'
          // label:'User',
          //  animation: google.maps.Animation.DROP
        });
        marker.setValues({type:"point", id: this.markersID.USER});
        const infowindow = new google.maps.InfoWindow({
          content: "Estás aquí"
        });
        marker.addListener('click', () => {
          infowindow.open(this.map, marker);
        })
        this.markers.push(marker);
      };
      PushMedicineAtencionMarker(puntosAtencion:any,callback:Function){
        this.DeleteMarkersById(this.markersID.ATENCION);
        //const imagen = 'assets/icon/positiva.png';

        const icon = {
          url: "assets/icon/positiva.svg", // url
          scaledSize: new google.maps.Size(50, 50), // scaled size
          origin: new google.maps.Point(0,0), // origin
          anchor: new google.maps.Point(0, 0) // anchor
      };
        let bounds = new google.maps.LatLngBounds();
        let points=[];
        for (let i = 0; i < puntosAtencion.length; i++) {
          let punto = { lat: +puntosAtencion[i].latitud, lng: +puntosAtencion[i].longitud }
          let point = puntosAtencion[i];


            point.coords= {
              lat: GoogleMapsProvider.NumberCultureInfo(puntosAtencion[i].latitud),
              lng: GoogleMapsProvider.NumberCultureInfo(puntosAtencion[i].longitud)

          };

          let marker = new google.maps.Marker({
            position: punto,
            map: this.map,
            icon: icon
          })
          let phoneNumber=puntosAtencion[i].telefono;
          if(phoneNumber==null){
            phoneNumber="Sin registro";
          }
          let infowindow = new google.maps.InfoWindow({
            content: "<div>" +
              "<br> <p>\ Dirección:\ " + puntosAtencion[i].direccion + "</p></div>"
            //  "</p> <br> <p> \ Teléfono:\ " + phoneNumber + "</p></div>"
          });

          marker.addListener('click', function () {
            infowindow.open(this.map, marker);
          });
          marker.setValues({type:"point", id: this.markersID.ATENCION});
          this.markers.push(marker);
          bounds.extend(marker.position);
          points.push(point);
      }

      this.map.fitBounds(bounds);
      this._resolveDistance(points).then(({elements})=>{

        elements.forEach((element, index) => {
          points[index].distance = element.distance.value
        });
        callback(points.sort((a, b) => a.distance - b.distance));
      });
    };


      PushMedicineRedMarker(redUrgencias:any,callback:Function){
        this.DeleteMarkersById(this.markersID.URGENCIAS);

        const icon = {
          url: "assets/icon/urgency.svg", // url
          scaledSize: new google.maps.Size(50, 50), // scaled size
          origin: new google.maps.Point(0,0), // origin
          anchor: new google.maps.Point(0, 0) // anchor
      };
        const points = [];
        let bounds = new google.maps.LatLngBounds();
        for (let i = 0; i < redUrgencias.length; i++) {
          const point = {
            name: redUrgencias[i].nombreSede,
            address: redUrgencias[i].direccion,
            phone: redUrgencias[i].telefono,
            coords: {
              lat:  redUrgencias[i].latitud,
              lng: redUrgencias[i].longitud
            }
          };

          let marker = new google.maps.Marker({
            position: point.coords,
            map: this.map,
            icon: icon
          });

          let infowindow = new google.maps.InfoWindow({
            content: `
              <div>
                <p>Nombre de sede: ${point.name}</p>
                <p>Dirección: ${point.address}</p>
                <!--p>Teléfono: ${point.phone}</p-->
              </div>
            `
          });

          marker.addListener('click', function () {
            infowindow.open(this.map, marker)
          });
          bounds.extend(marker.position);
          points.push(point);
          marker.setValues({type:"point", id: this.markersID.URGENCIAS});
          this.markers.push(marker);
        }
        this.map.fitBounds(bounds);

        this._resolveDistance(points).then(({elements})=>{

          elements.forEach((element, index) => {
            points[index].distance = element.distance.value
          });
          callback(points.sort((a, b) => a.distance - b.distance));
        });


      }

      _resolveDistance (points:any) {
        const distanceService = new google.maps.DistanceMatrixService()

        return new Promise((resolve, reject) => {
          distanceService.getDistanceMatrix({
            origins: [ {
              lat: this.coordenates.coords.latitude,
              lng: this.coordenates.coords.longitude
            }],
            destinations: points.map(({ coords }) => coords),
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            durationInTraffic: true,
            avoidHighways: false,
            avoidTolls: false
          }, ({ rows }, status) => {
            if (status !== google.maps.DistanceMatrixStatus.OK) {
              return reject(status)
            }

            resolve(rows[0])
          });
        });
      }

      loadMap(mapInstance,callback){

        this.CurrentPosition( _=> {

          if(google!==undefined){






            this.mapOptions.center= new google.maps.LatLng(
              this.coordenates.coords.latitude,
              this.coordenates.coords.longitude);

            this.mapOptions. mapTypeId=google.maps.MapTypeId.ROADMAP;


            this.map = new google.maps.Map(mapInstance, this.mapOptions);

            this.googleMapsEvents.dragEnd= google.maps.event.addListener(this.map, 'dragend', ()=> {


           //   this.EventDragEnd();
            });

            this.googleMapsEvents.idle= google.maps.event.addListenerOnce(this.map, 'idle', ()=>{

              callback(true);
              this.PushMarkerUserInMap(this.coordenates.coords.latitude,this.coordenates.coords.longitude);

            });







          }else{
            callback(false);
          }
        });

      }







      RemoveGoolgeMapsEvents () {
        this.map=null;
        google.maps.event.removeListener(this.googleMapsEvents.dragEnd);
        google.maps.event.removeListener(this.googleMapsEvents.idle);
      };



      GetAddress(Latitud,Longitud,callback) {
        let geocoder = new google.maps.Geocoder;
        let latLngSend = new google.maps.LatLng(GoogleMapsProvider.NumberCultureInfo(Latitud), GoogleMapsProvider.NumberCultureInfo(Longitud));
        geocoder.geocode({'location': latLngSend},  (results, status)=> {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              let address = results[0].formatted_address;
              callback(address,results);
            }
          }
        });
      };




      getLocationFromAddress (address,cb,error) {

        let geocoder = new google.maps.Geocoder;
        geocoder.geocode({'address': address}, (results, status)=> {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results.length > 0) {
              //console.info(JSON.stringify(results[0].geometry.location));
              cb(results[0].geometry.location);
            }
            else
            {
              error();
            }

          } else {
            console.log('Geocode was not successful for the following reason: ' + status);
            error();
          }
        });
      };











      DeleteMarkers () {
        for (let i = 0; i < this.markers.length; i++) {
          this.markers[i].setMap(null);
        }
        this.markers=[];
      };

      DeleteMarkersById (id:number) {
        let makerDelete= this.markers.filter( (filter)=> {
          return filter.id===id;
        });
        for (let i = 0; i < makerDelete.length; i++) {
          makerDelete[i].setMap(null);
          this.markers.splice(i,1);
        }
      };




      CurrentPosition(callback:Function) {


        this.geolocation.getCurrentPosition().then(position =>{
            this.coordenates=position;
            callback(position);

        }).catch(error=>{
            console.error(error);
            callback(false);
            switch (error.code) {
              case 1:
                  console.log('Permission Denied');
                  break;
              case 2:
                  console.log('Position Unavailable');
                  break;
              case 3:
                  console.log('Timeout');
                  break;
                }
          });

      }




}
