
import { Injectable } from '@angular/core'
import { AlertController } from 'ionic-angular';



@Injectable()
export class AlertsService {


  constructor(
    private alertCtrl:AlertController
  ) {

  }

  OneButton(message:string,buttonTextOrCallback?:any,callback?:Function,enableBackdropDismiss?:boolean){
    let button="Aceptar";
    let enableBackdropDismissValidation=false;
    if(enableBackdropDismiss){
      enableBackdropDismissValidation=true;
    }
    if(buttonTextOrCallback==undefined){
      if(typeof buttonTextOrCallback === 'string'){
        button=buttonTextOrCallback;
      }
    }
    let alert = this.alertCtrl.create({
      message:message,
      enableBackdropDismiss:enableBackdropDismissValidation,
      buttons: [{
        text: button,
        handler: data => {
          if(typeof buttonTextOrCallback === 'function'){
            buttonTextOrCallback();
          }
          if(callback!==undefined){
            callback();
          }
        }
      }],
      cssClass: "positiva-alert"
    });

    alert.present();
  }


  TowButtons(message:string,successButtonText:string,cancelButtonText:string,success:Function,fail?:Function){

    let alert = this.alertCtrl.create({
      message:message,
      enableBackdropDismiss:false,
      buttons: [{
        text: successButtonText,
        handler: _ => success()
        },{
          text: cancelButtonText,
          handler: _ => {
            if(fail!==undefined){
              fail();
            }
          }
        }],
      cssClass: "positiva-alert"
    });

    alert.present();
  }

  Fingerprint(callback:Function){

    let alert = this.alertCtrl.create({
      message:`
      <img src="assets/imgs/fingerprint.svg" width="125">

      <h3 class="color1">Autenticación por huella</h3>
      <p>¿Desea en próximas ocasiones utilizar la verificación por huella para entrar a la aplicación?</p>`,
      enableBackdropDismiss:false,
      cssClass: "positiva-alert"
    });
    alert.addInput({
      type: 'checkbox',
      label: 'No volver a preguntar',
      value: "true",
      checked: false

    });
    alert.addButton({
      text:'Cancelar',
      handler:(data:any) =>{
        console.log(data);
        if(data[0]=="true" ){
      callback(false,true);

      }else if(data.length==0){
        callback(false,false);
      }
      else{
        return false;
      }
  }
  });
  alert.addButton({
    text:'Aceptar',
    handler:(data:any) =>{
      console.log(data);
      if(data[0]=="true" ){
    callback(true);

    }else if(data.length==0){
      callback(true);
    }
    else{
      return false;
    }
}
})

    alert.present();
  }


  update(forceUpdate:boolean,callback:Function){
    let alert = this.alertCtrl.create({
      title:"Actualización disponible",
      enableBackdropDismiss:false,
      buttons: [{
        text: "Actualizar",
        handler: data => {
         console.log("Go to market place");
         alert.dismiss(true);
         return false;


        }
      }]
    })
    let message="Actualización necesaria, dirigete la a tienda de aplicaciones y actualiza a la última versión."
    if(!forceUpdate){
      message="Actualización disponible, si quieres disfrutar de nuevas funcionalidades y mejoras en redimiento te recomendamos actualizar."
      alert.addButton("Cancelar");

    }
    alert.setMessage(message);
    alert.onWillDismiss((data)=>{
      callback(data);
    });




    alert.present();
  }


  RelacionLaboral(relaciones,callback:Function){
    let relacionAlert = this.alertCtrl.create({
      cssClass: "custom-alert positiva-alert positiva-radio positiva-company",
      enableBackdropDismiss:false
    });

    relacionAlert.setMessage(`<h3 class="color1">Selecciona tu relacion laboral</h3>`);
   for(let i=0;i<relaciones.length;i++){

    relacionAlert.addInput({
      type: 'checkbox',
      label: relaciones[i].razonSocial,
      value: JSON.stringify(i),

      handler:data=>{
       // relacionAlert.data.inputs[JSON.parse(data.value)].checked=true;
        if(data.checked){

          for(let i=0;i<relaciones.length;i++){
            if(JSON.parse(data.value)!==i){
              relacionAlert.data.inputs[i].checked=false;
            }

          }
        }
      }
    });
   }

   relacionAlert.addButton({
     text: 'CANCELAR',
     role: 'cancel'
   })

  relacionAlert.addButton({
    text:'ACEPTAR',
    handler:(data:any) =>{
      console.log(data);
      if(data.length>0 ){
    callback(relaciones[JSON.parse(data[0])]);
    }
    else{
      return false;
    }
}
})

relacionAlert.present()
  }

  register(callback:Function){
    let terminosAlert = this.alertCtrl.create({
      cssClass: "custom-alert positiva-alert positiva-radio"
    });
  terminosAlert.setMessage('  <img src="assets/icon/warning.svg" width="125">  <h3 class="color1">Apreciado(a) asegurado(a): </h3><p>Autorizas el tratamiento de tus datos personales de acuerdo a la ley 1581 de 2012 la cual podras consultar en nuestro sitio web</p>');
  terminosAlert.addInput({
    type: 'radio',
    label: 'Acepto',
    value: "true",
    checked: false

  });
  terminosAlert.addInput({
    type: 'radio',
    label: 'No acepto',
    value: "false",
    checked: false

  });

  terminosAlert.addButton({
    text:'CONTINUAR',
    handler:(data:any) =>{
      console.log(data);
      if(data=="true" ){
    callback(true);

    }else if(data=="false"){
      callback(false);
    }
    else{
      return false;
    }
}
})

terminosAlert.present()
  }


}
