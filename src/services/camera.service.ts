
import { Injectable } from '@angular/core'
import { Camera, CameraOptions,CameraPopoverOptions } from '@ionic-native/camera';

@Injectable()
export class CameraService {
  options:CameraOptions;
  popoverOptions:CameraPopoverOptions
  constructor(
      private camera:Camera
    ) {
      this.popoverOptions={
        width:180,
        height:240,
        x:0,
        y:0,
        arrowDir:1
      }
       this.options = {
        quality: 10,
        destinationType: this.camera.DestinationType.NATIVE_URI,
        sourceType:this.camera.PictureSourceType.CAMERA,
        allowEdit:true,
        encodingType: this.camera.EncodingType.JPEG,
       // mediaType: this.camera.MediaType.PICTURE,
        targetWidth:100,
        targetHeight:100,
        cameraDirection:this.camera.Direction.BACK,
        correctOrientation:false,
        saveToPhotoAlbum:true,
        popoverOptions:this.popoverOptions
      }
      }

    TakePhoto(callback,options?:CameraOptions){
      if(options!==undefined){
        this.options=options;
      }
      this.camera.getPicture(options).then((imageData) => {
        callback(imageData);
       }, (err) => {
        console.error(err);
       });
    }

 
}
