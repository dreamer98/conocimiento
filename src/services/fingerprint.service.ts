import { Injectable } from '@angular/core';
import { KeychainTouchId } from '@ionic-native/keychain-touch-id';
import { AlertsService } from './alerts.service';


@Injectable()
export class FingeprintService {
  hasId:boolean=false;
  hasData:boolean=false;

  constructor(private idVerification:KeychainTouchId,
    private alerts:AlertsService) {

  }

  HasIdData(callback:Function){
    let askAgain=JSON.parse(window.localStorage.getItem("fingerprintAsk"));
    if(askAgain==null){
      window.localStorage.setItem("fingerprintAsk",JSON.stringify(true));
    }
    this.idVerification.isAvailable()
    .then((res: any) => {
      console.log(res);
      this.hasId=true;
      this.idVerification.has("positiva")
      .then(succes=>{
        this.hasData=true;
        console.log(succes);
          callback();
      }).catch(reject=>{
         callback();
        console.log(reject);
      })
    })
    .catch((error: any) =>{
      callback();
    console.error(error);});


  }




  FingerPrintValidation(callback:Function,message?:string){
    let fingerprintMessage="Ingrese a positiva utilizando su huella";
    if(message!==undefined){fingerprintMessage=message;}
    this.idVerification.verify("positiva",fingerprintMessage)
    .then(verify=>{
      console.log(verify);
      callback(true,JSON.parse(verify));
    })
    .catch(notVerify=>{
      callback(false);
      console.log(notVerify);
    })
  }

  FingerprintSaveLogin(data:any,callback:Function){
    console.log(JSON.parse(window.localStorage.getItem("fingerprintAsk")),this.hasData);
    if(JSON.parse(window.localStorage.getItem("fingerprintAsk")) && !this.hasData){
      this.alerts.Fingerprint((val:boolean,askAgain:boolean)=>{
        if(val){
          this.idVerification.save("positiva",JSON.stringify(data)).then(data=>{
            console.log(data);
            this.hasData=true;
            callback();
          })
          .catch(notVerify=>{
           callback();
            console.log(notVerify);
          })

        }  else {
            callback();
            if(askAgain){
               window.localStorage.setItem("fingerprintAsk",JSON.stringify(false));
            }else{
              window.localStorage.setItem("fingerprintAsk",JSON.stringify(true));
            }

        }

    });
    }else{
      callback();
    }



  }

  DeleteFingerprintData(callback:Function){
    this.alerts.TowButtons(
      `<img src="assets/imgs/fingerprint.svg" width="125">    
    <h3 class="color1">Autenticación por huella</h3>
    <p>¿Estas seguro que deseaa borrar los datos actuales de la huella?</p>`,
      "SÍ","Cancelar",
  _=>{
     this.FingerPrintValidation(verify=>{
       if(verify){
         this.idVerification.delete("positiva").then(deleted=>{
          this.hasData=false;
          console.log(deleted);
           window.localStorage.removeItem("fingerprintAsk");
          callback();
         })
         .catch(errorDeleted=>{
           console.log(errorDeleted);
         })
       }
     },"Acerque la huella para borrar los datos.");
  });
  }





}
