import { Injectable } from '@angular/core'
import { NetworkInterface } from '@ionic-native/network-interface';
import { Network } from '@ionic-native/network';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';


@Injectable()
export class UserDataService {
    carnetActivo:boolean;
    empleadoresActivos:empleadores[];
    private phoneCodeEnum={
        LINEA_ORO:"LINEA_ORO",
        FISCALIA:"FISCALIA",
        RAMA_JUDICIAL:"FISCALIA",
        GENERAL:"GENERAL"
      }
    data: {
        fuenteAcceso: string,
        funcionalidades: funcionalidades[],
        informacionPersonal: {

            aceptaTerminos: boolean,
            correoElectronico: string,
            direccion: string,
            empleadores: empleadores[],
            fechaNacimiento: string,
            id: number,
            numeroDocumento: string,
            primerApellido: string,
            primerNombre: string,
            segundoApellido: string,
            segundoNombre: string,
            telefonoFijo: string,
            telefonoMovil: string,
            tipoDocumento: string
        }

    }
constructor (
  private $network: Network,
  private $networkInterface: NetworkInterface,
  private $geolocation: Geolocation,
  private $platform: Platform) {}

  async getLoginInfo () {
    const { coords } = await this.$geolocation.getCurrentPosition()
    if( !this.$platform.is('mobileweb')) {

   

    return {
      latitud: coords.latitude,
      longitud: coords.longitude,
      tipoConexion: this.$network.type,
      ip: (
        await this.$networkInterface.getWiFiIPAddress() ||
        await this.$networkInterface.getCarrierIPAddress()
      ).ip,

      // TODO: Get specific platform
      plataforma: this.$platform.is('ios') ? 'ios' :
        this.$platform.is('android') ? 'android' : 'browser',
    }
  }else{
      return{
        latitud: coords.latitude,
        longitud: coords.longitude,
        tipoConexion: "ETHERNET",
        ip: "0.0.0.0",
        plataforma: 'browser',
      }
  }

  }

setUserData(data:any){
    this.data=data;

    this.carnetActivo=this.data.informacionPersonal.empleadores.some(empleador => empleador.activa);
    this.empleadoresActivos=this.data.informacionPersonal.empleadores.filter(empleador => empleador.activa);
    this.empleadoresActivos.map(empleadores=>{

         let razon=empleadores.razonSocial.split(" ")[0].toUpperCase();

         switch(razon){
             case "FISCALIA":
             empleadores.phoneCode =   this.phoneCodeEnum.FISCALIA;
             break;
             case "RAMA":
               empleadores.phoneCode = this.phoneCodeEnum.RAMA_JUDICIAL;
             break;
             case "POSITIVA":
               empleadores.phoneCode = this.phoneCodeEnum.LINEA_ORO;
               break;
             default:
              empleadores.phoneCode = this.phoneCodeEnum.GENERAL;
              if(empleadores.marcaEmpresa.topMil){
                empleadores.phoneCode = this.phoneCodeEnum.LINEA_ORO;
             }
             break;
         }




    });
    console.log(this.empleadoresActivos);

}

clearData(){
    this.data=null;
}
}

class funcionalidades {
    id: number;
    codigo: string
}


 class empleadores {

    activa: boolean;
    departamentoCode: string;
    direccion: string;
    marcaEmpresa: {
        vip: boolean,
        sgp: boolean,
        inconsistentes: boolean,
        topMil: boolean,
        cobre: boolean,
        plata: boolean,
        bronce: boolean
    };
    municipioCode: string;
    numeroDocumento: string
    razonSocial: string;
    tipoDocumento: string;
    phoneCode:string;

}
