
import { Injectable } from '@angular/core'
import { AppRate } from '@ionic-native/app-rate';
import { SocialSharing } from '@ionic-native/social-sharing';

@Injectable()


export class ShareRateServiceProvider {
    appRate: any = AppRate;
   constructor(private appRateService:AppRate,private socialSharing:SocialSharing){


   }

   RateApp(){
    this.appRateService.preferences = {
        displayAppName: 'My custom app title',
        usesUntilPrompt: 5,
        promptAgainForEachNewVersion: false,
        inAppReview: true,
        storeAppURL: {
          ios: '1195356641',
          android: 'market://details?id=com.multisecurityservices.positiva'
        },
        customLocale: {
          title: "Would you mind rating %@?",
          message: "It won’t take more than a minute and helps to promote our app. Thanks for your support!",
          cancelButtonLabel: "No, Thanks",
          laterButtonLabel: "Remind Me Later",
          rateButtonLabel: "Rate It Now",
          yesButtonLabel: "Yes!",
          noButtonLabel: "Not really",
          appRatePromptTitle: 'Do you like using %@',
          feedbackPromptTitle: 'Mind giving us some feedback?',
        },
        callbacks: {
          handleNegativeFeedback: function(){
           console.log("feedback");
          },
          onRateDialogShow: function(callback){
           console.log("callback",callback);
           
          },
          onButtonClicked: function(buttonIndex){
            console.log("onButtonClicked -> " + buttonIndex);
          }
        }
      };
      this.appRateService.promptForRating(false);

   }

   Share(){
     this.socialSharing.shareViaFacebookWithPasteMessageHint("comparte positiva app",null,"www.gmail.com","Comparte Positiva")
     .then(data=>console.log(data))
     .catch(err=>console.error(err));
   }


}