import env from '../env'

import { Injectable } from '@angular/core'

@Injectable()
export class StoreService {
  prefix: string
  storage: Storage

  constructor() {
    this.prefix = env.STORE_PREFIX

    // Also could be sessionStorage
    this.storage = window.localStorage
  }

  private _getKey(key: string) {
    return `${this.prefix}:${key}`
  }

  get(key: string): any {
   const data =JSON.parse(localStorage.getItem("positiva-app:user"))
     return data
  }

   set(key: string, value: any) {
    this.storage.setItem(this._getKey(key), JSON.stringify(value))
   
  }

  remove(key: string) {
    this.storage.removeItem(this._getKey(key))
  }

   getParse(key:string){
    const data =JSON.parse(localStorage.getItem("positiva-app:user"))
    return  JSON.parse(JSON.parse(decodeBase64Unicode(data.split('.')[1])).user)
  }

  clear() {
    // We cannot call storage.clear directly since we need
    // only to delete out our isolated data (prefixed)
    let i = this.storage.length

    while (i--) {
      const key = this.storage.key(i)

      if (key.startsWith(this.prefix)) {
        this.storage.removeItem(key)
      }
    }
  }
}

// Taken from: https://stackoverflow.com/a/30106551
function decodeBase64Unicode (str) {
  return decodeURIComponent(Array.prototype.map.call(atob(str), c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''))
}
