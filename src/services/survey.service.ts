
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

export interface Survey {
  id: number
  nombre: string
  descripcion: string
  fechaInsert: number
  fechaUpdate: number
  active: boolean
  calificarServicio: boolean
  preguntas: Question[]
}

export interface Question {
  id: number
  descripcion: string
  tipoPregunta: {
    id: number
    nombre: string
  }
  fechaInsert: number
  active: boolean
  opcionesRespuesta: Answer[]
}

export interface Answer {
  id: number
  descripcion: string
}

export interface SurveyCompleted {
  servicio: {
    servicioCuidaId: number
    codigo: string
    descripcion: string
  }
  preguntas: Array<{
    id: number
    comentario: string
    opcionesRespuesta: Answer[]
  }>
}

@Injectable()
export class SurveyService {
  constructor(
    private $http: HttpClient,
    ) {}

  get(): Promise<Survey> {
    return this.$http.get('/encuestas/calificar-servicio').toPromise()
      // Get onlye the first one
      .then(({ data }: any) => data)
  }

  apply(id: number, survey: SurveyCompleted): Promise<any> {
    return this.$http.post(`/encuestas/${id}/respuesta`, survey).toPromise()
    //return this.$http.post(`@beneficiario/surveys/${data.survey_id}/instances`, data).toPromise()
  }
}
