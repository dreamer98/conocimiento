import { Component, Output, EventEmitter, OnInit } from '@angular/core'
import { StoreService } from '../../services/store.service'
import { LoggerService } from '../../services/logger.service'

@Component({
  selector: 'companies-dropdown',
  templateUrl: 'companies-dropdown.html'
})
export class CompaniesDropdownComponent implements OnInit {
  @Output('onCompanyChange') change = new EventEmitter<any>()

  companies: any[]
  activeCompany: any

  constructor(
    private $store: StoreService,
    private $logger: LoggerService) {
    const user = $store.getParse('user')

    this.companies = CompaniesDropdownComponent.getActiveCompanies(user.informacionPersonal.empleadores)
    this.activeCompany = $store.get('active-company') || this.companies[0]
  }

  static getActiveCompanies(companies: any[]) {
    return companies.filter(company => company.activa)
  }

  private _saveActiveCompany() {
    this.$store.set('active-company', this.activeCompany)
    this.change.emit(this.activeCompany)
  }

  ngOnInit() {
    this._saveActiveCompany()
  }

  onCompanyChange(company) {
    this.$logger.log('Changing to company:', company)
    this._saveActiveCompany()
  }
}
