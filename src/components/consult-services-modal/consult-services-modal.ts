import { Component } from '@angular/core'

import { NavParams, ViewController } from 'ionic-angular'


@Component({
  selector: 'consult-services-modal',
  templateUrl: 'consult-services-modal.html'
})
export class ConsultServicesModalComponent {
  request: any

  constructor(
    $navParams: NavParams,
    private $viewCtrl: ViewController,

      ) {
    this.request = $navParams.get('request')
    console.log(this.request)
  }

  dismiss() {
    this.$viewCtrl.dismiss()
  }
}
