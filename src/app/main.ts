import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'

import { AppModule } from './app.module'

import { ToastController,LoadingController } from 'ionic-angular'
import { decorateToast } from '../utils/decorate-toast'
import {decorateLoading} from '../utils/decorate-loading'

decorateToast(ToastController)
decorateLoading(LoadingController)

platformBrowserDynamic().bootstrapModule(AppModule)
