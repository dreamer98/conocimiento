import { HomePage } from './../pages/home/home';
import { ItemsService } from './../services/items.service';
import { AlertsService } from './../services/alerts.service';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController,Events  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { App } from 'ionic-angular';
import { LoginPage } from '../pages/login/login'
import { Market } from '@ionic-native/market';
import { AppVersion } from '@ionic-native/app-version';
import * as env from '../env';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  UATEnvironment:boolean=false;
  rootPage: any = LoginPage
  enviroment:string="";
  @ViewChild('myNav') nav: NavController
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, app:App,
    events:Events,alerts:AlertsService, market:Market, version:AppVersion,
    ItemsService:ItemsService
  ) {
    platform.ready().then(() => {


    if( !platform.is('mobileweb')) {


      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString('#ff7500');
      splashScreen.hide();
      events.subscribe('setRoot', (page,params) => {
      console.log(params);
        this.nav.setRoot(page,params);
      });
      platform.registerBackButtonAction(() => {

        let nav = app.getActiveNavs()[0];



            if (nav.canGoBack()){ 
                nav.pop();
            } else  {
             events.publish("OpenPage",HomePage);
            }

    });
      version.getVersionNumber().then(appVersion=>{
        ItemsService.getVersionMin().then(data=>{
          console.log("Server Version ",data.version);
          console.log("Current Version ",appVersion);
        let versionAct =data.version;
        let versionServer = data.version;
      if(versionAct!==versionServer){
        if(Number(versionServer.split(".")[0])>Number(versionAct.split(".")[0])){
            alerts.update(true,update=>{
              if(update){
                market.open("com.multisecurityservices.positiva");
              }
               });
        }else if(Number(versionServer.split(".")[1])>Number(versionAct.split(".")[1])){
            alerts.update(false,update=>{
              if(update){
                market.open("com.multisecurityservices.positiva");
               }
            });
        }
      }

    });
      });
    }
   
      if(env.default.IN_DEVELOPMENT){
        this.enviroment="Pruebas Testing";

      }else{
        this.enviroment="Pruebas UAT";
      }

      setInterval(()=>{
        this.UATEnvironment=!this.UATEnvironment;
     },1000);


    });
  }


}
