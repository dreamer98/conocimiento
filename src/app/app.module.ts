import { UserDataService } from './../services/userData.service';
import { FingeprintService } from './../services/fingerprint.service';
import { AppRate } from '@ionic-native/app-rate';
import { ShareRateServiceProvider } from '../services/rate-share.service';
import { BrowserModule } from '@angular/platform-browser'
import {  NgModule } from '@angular/core'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { Geolocation } from '@ionic-native/geolocation';
import { IonicApp,  IonicModule } from 'ionic-angular'
import { SplashScreen } from '@ionic-native/splash-screen'
import { StatusBar } from '@ionic-native/status-bar'
import { HttpClientModule } from '@angular/common/http'
import { HomePage } from "../pages/home/home";
import { MainPage } from "../pages/main/main";
import { LoginPage } from "../pages/login/login";
import { RegisterPage} from "../pages/register/register";
import {CarnetPage} from "../pages/carnet/carnet";
import {RedUrgenciasPage} from "../pages/red-urgencias/red-urgencias";
import {CitasRehabilitacionPage} from "../pages/citas-rehabilitacion/citas-rehabilitacion";
import {IncapacidadesTemporalesPage} from "../pages/incapacidadesTemporales/incapacidadesTemporales";
import {IncapacidadesPermanentesPage} from "../pages/incapacidadesPermanentes/incapacidadesPermanentes";
import {PqrdPage} from  "../pages/pqrd/pqrd";
import {AsistenciaTelefonicaPage} from "../pages/asistencia-telefonica/asistencia-telefonica";
import {PerfilPage} from "../pages/perfil/perfil";
import {InformacionInteresPage} from "../pages/informacion-interes/informacion-interes";
import {PuntosAtencionPage} from "../pages/puntos-atencion/puntos-atencion";
import {CodigoPage} from "../pages/codigo/codigo";
import {DetallesIncapacidadesPage} from "../pages/detallesIncapacidades/detallesIncapacidades";
import {NuevoPqrdPage} from "../pages/nuevoPqrd/nuevoPqrd";
import { ConsultaAutorizacionMedicaPage } from '../pages/consulta-autorizacion-medica/consulta-autorizacion-medica'
import { NewInfoPage } from '../pages/new-info/new-info'
import { NewRequestPage } from '../pages/new-request/new-request'
import { SurveyPage } from '../pages/survey/survey'
import { EvaluateServicesPage } from '../pages/evaluate-services/evaluate-services'
import { MyApp } from './app.component'
import { CompaniesDropdownComponent } from '../components/companies-dropdown/companies-dropdown'
import { ConsultServicesModalComponent } from '../components/consult-services-modal/consult-services-modal'
import { GoogleMaps} from '@ionic-native/google-maps'
import { Camera } from '@ionic-native/camera'
import { AuthenticationService } from '../services/authentication.service'
import { ItemsService} from '../services/items.service'
import { FileOpener } from '@ionic-native/file-opener'
import { File } from '@ionic-native/file'
import { FileTransfer } from '@ionic-native/file-transfer'
import { StoreService } from '../services/store.service'
import { ConsultService } from '../services/consult.service'
import { LoggerService } from '../services/logger.service'
import { SurveyService } from '../services/survey.service'
import { HttpLoggerInterceptor } from '../interceptors/http-logger.interceptor'
import { AndroidPermissions} from '@ionic-native/android-permissions'
import { LoadingService } from '../services/loading.service';
import { AlertsService } from '../services/alerts.service';
import { CameraService } from '../services/camera.service';
import { ToastService } from '../services/toast.service';
import { GoogleMapsProvider } from '../services/google-maps.service';
import { EncodeHTML } from '../pipes/encode-html.pipe';
import { Market } from '@ionic-native/market';
import { AppVersion } from '@ionic-native/app-version';
import { SolicitudAutorizacionMedicaPage } from '../pages/solicitud-autorizacion-medica/solicitud-autorizacion-medica';
import { SocialSharing } from '@ionic-native/social-sharing';
import { KeychainTouchId } from '@ionic-native/keychain-touch-id';
import { NetworkInterface } from '@ionic-native/network-interface'
import { Network } from '@ionic-native/network'

@NgModule({
  declarations: [
    MyApp,
    MainPage,
    HomePage,
    LoginPage,
    CitasRehabilitacionPage,
    RegisterPage,
    RedUrgenciasPage,
    IncapacidadesPermanentesPage,
    IncapacidadesTemporalesPage,
    CarnetPage,
    PqrdPage,
    AsistenciaTelefonicaPage,
    PuntosAtencionPage,
    PerfilPage,
    CodigoPage,
    NuevoPqrdPage,
    DetallesIncapacidadesPage,
    InformacionInteresPage,
    ConsultaAutorizacionMedicaPage,
    CompaniesDropdownComponent,
    ConsultServicesModalComponent,
    NewInfoPage,
    SurveyPage,
    NewRequestPage,
    EvaluateServicesPage,
    EncodeHTML,
    SolicitudAutorizacionMedicaPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Atrás',
      mode: 'ios',
      scrollAssist: false,
      autoFocusAssist:false

    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage,
    HomePage,
    LoginPage,
    CitasRehabilitacionPage,
    RegisterPage,
    RedUrgenciasPage,
    IncapacidadesPermanentesPage,
    IncapacidadesTemporalesPage,
    CarnetPage,
    PqrdPage,
    PuntosAtencionPage,
    AsistenciaTelefonicaPage,
    PerfilPage,
    CodigoPage,
    NuevoPqrdPage,
    DetallesIncapacidadesPage,
    InformacionInteresPage,
    ConsultaAutorizacionMedicaPage,
    ConsultServicesModalComponent,
    NewInfoPage,
    SurveyPage,
    NewRequestPage,
    EvaluateServicesPage,
    SolicitudAutorizacionMedicaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ItemsService,
    Geolocation,
    GoogleMaps,
    AuthenticationService,
    StoreService,
    ConsultService,
    FileOpener,
    FileTransfer,
    File,
    Camera,
    AndroidPermissions,
    LoggerService,
    SurveyService,
    LoadingService,
    AlertsService,
    CameraService,
    ToastService,
    GoogleMapsProvider,
    Market,
    AppVersion,
    //{ provide: HTTP_INTERCEPTORS, useClass: EndpointInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpLoggerInterceptor, multi: true },
    ShareRateServiceProvider,
    AppRate,
    SocialSharing,
    KeychainTouchId,
    FingeprintService,
    UserDataService,
    NetworkInterface,
    Network
  ]
})
export class AppModule {}
