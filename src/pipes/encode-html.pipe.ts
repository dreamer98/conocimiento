import { Pipe, PipeTransform } from '@angular/core'

/**
 * Encodes input to HTML entities
 */
@Pipe({ name: 'encodeHTML' })
export class EncodeHTML implements PipeTransform {
  static CONSONANTS_REGEX = /ñ/gi
  static VOWELS_REGEX = /áéíóú/gi

  static CONSONANTS = {
    'Ñ': 'N',
    'ñ': 'n'
  }

  static VOWELS = {
    'Á': 'A',
    'á': 'a',

    'É': 'E',
    'é': 'e',

    'Í': 'I',
    'í': 'i',

    'Ó': 'O',
    'ó': 'o',

    'Ú': 'U',
    'ú': 'u'
  }

  transformConsonant (consonant: string): string {
    return `&${EncodeHTML.CONSONANTS[consonant]}tilde;`
  }

  transformVowel (vowel: string): string {
    return `&${EncodeHTML.VOWELS[vowel]}acute;`
  }

  transform (value: string): string {
    return value
      .replace(EncodeHTML.CONSONANTS_REGEX, this.transformConsonant)
      .replace(EncodeHTML.VOWELS_REGEX, this.transformVowel)
  }
}
