// TODO: check https://medium.com/beautiful-angular/angular-2-and-environment-variables-59c57ba643be

export default {
  IN_DEVELOPMENT: false,


  get API_ROOT () {
    return this.IN_DEVELOPMENT
      ? 'https://beneficiariotesting.conexia.com.co/positiva/api'
      : 'https://beneficiariouatbackend.conexia.com.co/positiva/api'
  },

  get BENEFICIARIO_UAT_API () {
    return this.IN_DEVELOPMENT
      ? 'http://192.168.140.13'
      : 'http://beneficiario.conexia.com.co'
    },

  get CUIDA_UAT_API () {
    return this.IN_DEVELOPMENT
      ? 'http://192.168.140.21/positiva-ws'
      : 'http://cuidauat.conexia.com.co/positiva-ws'
  },
  get BACKEND_UAT_API (){
    return this.API_ROOT.replace('/positiva/api', '')
  },

  // Used as sotrage key
  STORE_PREFIX: 'positiva-app',

  // Log level by default
  LOG_LEVEL: 3,

  // When attemps to log some http info
  SHOW_HTTP_LOGS: true
}
