import { AlertsService } from './../../services/alerts.service';
import { LoadingService } from './../../services/loading.service';
import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, ModalController, Content } from 'ionic-angular';
import {ItemsService} from '../../services/items.service';
import { NewInfoPage } from '../new-info/new-info'
import {MainPage} from '../main/main'

@Component({
  selector: 'page-informacion-interes',
  templateUrl: 'informacion-interes.html'

})
export class InformacionInteresPage {
  static SCROLL_TRESHOLD = 100

  @ViewChild(Content) content: Content

  titulo = []

  scrollIndicator: boolean = false

  constructor(
    public navCtrl: NavController,
    public _ItemsService: ItemsService,
    private $modalCtrl: ModalController,
    private $zone: NgZone,
    private loading:LoadingService,
    private alerts:AlertsService) {
    
  }

  _calcIndicatorState () {
    return (
      this.content.scrollTop + this.content.contentHeight <
      this.content.getContentDimensions().scrollHeight - InformacionInteresPage.SCROLL_TRESHOLD
    )
  }

  toggleIndicator () {
    this.$zone.run(() => {
      this.scrollIndicator = this._calcIndicatorState()
    })
  }

  openInfo(notice) {
    this.$modalCtrl.create(NewInfoPage, { data: notice }).present()
  }
  ionViewDidLoad(){
  this.navCtrl.insert(0,MainPage);
  this.loading.Normal();
  this._ItemsService.noticias().then(responds=> {
    this.scrollIndicator = this._calcIndicatorState();
    this.loading.Hide();
    if(responds.data.length >0){
            //wwww.benficiario.conexia.com.co/backend/public
            
      this.titulo=responds.data;// =  {titulo:responds[i].title ,descripcion:responds[i].description,contenido:responds[i].html_content ,imagen:"http://beneficiario.conexia.com.co/backend/public/"+responds[i].main_image}
    }else{
      this.alerts.OneButton(`
      <img src="assets/icon/warning.svg" width="125">
      <h3 class="color1">Error al cargar noticias</h3>
      <p>Por favor intenta más tarde</p>`)
    }

    
     
 
  })

}
}
