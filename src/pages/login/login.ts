import { User } from './../../definitions/user';
import { FingeprintService } from './../../services/fingerprint.service';
import { Component, NgZone } from '@angular/core'
import { NavController, Platform, NavParams } from 'ionic-angular';
import { AuthenticationService } from '../../services/authentication.service'
import { AndroidPermissions } from '@ionic-native/android-permissions'
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterPage } from "../register/register";
import { CodigoPage } from "../codigo/codigo"
import { MainPage } from '../main/main'
import { ITUser } from '../../definitions/interfaces/it_user'
import { StoreService } from '../../services/store.service';
import { LoadingService } from '../../services/loading.service';
import { AlertsService } from '../../services/alerts.service';
import { ToastService } from '../../services/toast.service';
import { UserDataService } from '../../services/userData.service';
import { NetworkInterface } from '@ionic-native/network-interface';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  model: User = new User(null, "", false);//1096235487
  userRespond: ITUser;
  validated = false;
  loginForm: FormGroup;
  passwordForm: FormGroup;
  activeHome = false

  constructor(
    private _navCtrl: NavController,
    private _authService: AuthenticationService,
    private androidPermissions: AndroidPermissions,
    private $store: StoreService,
    private formBuilder: FormBuilder,
    private loading: LoadingService,
    private alerts: AlertsService,
    private toast: ToastService,
    private navParams: NavParams,
    private platform: Platform,
    public fingerprintService: FingeprintService,
    private $zone: NgZone,
    private userData:UserDataService,
    private $network: NetworkInterface
  ) {
    const global = platform.win()


    global.addEventListener('keyboardWillShow', e =>  {
      console.log('Opening keyboard', e)
      this.$zone.run(() => {
        this.activeHome = true
      })
    });

    global.addEventListener('keyboardWillHide', e => {
      console.log('Closing keyboard', e)

      this.$zone.run(() => {
        this.activeHome = false
      })
    })

    this.$network.getWiFiIPAddress()
      .then(address => {
        console.log('Address IP:', address)
      })
      .catch(error => {
        console.log(error)
      })

    this.platform.ready().then(() => {


      this.fingerprintService.HasIdData(() => {

        if (this.fingerprintService.hasId && this.fingerprintService.hasData) {
          this.fingerprintService.FingerPrintValidation(
            (verify, data) => {
              if (verify) {
                this.model = data;
                this.enter(true);
              }
            });
        }
      });

    });



    let showExpired = this.navParams.get('showMessage');
    if (showExpired != undefined && showExpired) {

      this.alerts.OneButton(`<img src="assets/icon/warning.svg" width="125">
        <h3 class="color1">Sesión expirada</h3>
        <p>Por favor vuelve a iniciar sesión.</p>`);
      this._navCtrl.insert(0, LoginPage);
    }


    this.loginForm = this.formBuilder.group({
      identificationInput: ['', Validators.compose([Validators.required,
      Validators.minLength(6),
      Validators.maxLength(12),
        // Validators.pattern('(?!0)\\d+')
      ])],

    });
    this.passwordForm = this.formBuilder.group({
      passwordInput: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])],

    });

    this.model.tipo_documento = "CC";

    if (this.platform.is('android') && !this.platform.is('mobileweb')) {
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
        result => console.log('has permission?', result.hasPermission),
        _ => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
      );
    }
  }
  ionViewDidLoad() {
    // this.model.password="123456"




  }



  login() {
    localStorage.removeItem('positiva-app:user');
    this.loading.Normal("Cargando..");


    this._authService.validate(this.model.tipo_documento, this.model.numero).then(responds => {
      console.log(responds.data)


      if (responds.data.hasOwnProperty("statusCode")) {
        switch (+responds.data.statusCode) {
          case 0:
            this.toast.ShowMessage(`El usuario con el documento ${this.model.numero} no se encuentra registrado en Positiva`);
            break;
          case 1:
            this._navCtrl.push(RegisterPage, { user: this.model, responds: responds.data });
            this.toast.ShowMessage("Por favor, regístrate antes de ingresar")
            break;
          case 2:
            this.validated = true;
            this.toast.ShowMessage("Ingresa tu contraseña")
            break;
          case 3:
            this._navCtrl.push(CodigoPage, { user: this.model });
            this.toast.ShowMessage("Verifica el código que fue enviado a tu correo y/o teléfono móvil")
        }
      }

      this.loading.Hide();

    })


  }

  AccessFingerprint() {
    this.fingerprintService.FingerPrintValidation((verify: boolean, data: User) => {
      if (verify) {
        this.model = data;
        this.enter(true);
      }
    })
  }

  logout() {
    this._authService.logout()
  }

  chooseAllField() {

    if (this.model.numero == null || this.model.tipo_documento == "")
      return true;
    else return false;

  }

  goBack() {
    this.validated = false;
    this.model.numero = null;
    this.model.password = null;

  }

  async enter(fromFingerprint?: boolean) {

    this.loading.Normal("Ingresando...")

    this._authService.autenticacion(this.model/*, await this.userData.getLoginInfo()*/)
      .then(respuesta => {
        if (respuesta.hasOwnProperty('data')) {
          this.$store.set('user', respuesta.data.token)
          let temp = this.$store.getParse("");
          this.userData.setUserData(temp);
          if (fromFingerprint || !this.fingerprintService.hasId) {
            this._navCtrl.setRoot(MainPage);
          } else if (this.fingerprintService.hasId && !this.fingerprintService.hasData) {
            this.fingerprintService.FingerprintSaveLogin(this.model,
              _ => {
                this._navCtrl.setRoot(MainPage);
              });
          } else {
            this._navCtrl.setRoot(MainPage);
          }

        } else {

          this.loading.Hide();
          this.toast.ShowMessage(respuesta.message);
        }
      })

  }

  recuperarClave() {
    this.alerts.OneButton(`
    <img src="PositivaWebApp/assets/icon/warning.svg" width="125">
    <h3 class="color1">Recuperar contraseña</h3>
    <p>Se ha enviado un correo y/o mensaje de texto a los datos registrados con el usuario: </p>
    <strong class="color1">${this.model.numero}</strong>
  `, "Enviar", () => {
        this._authService.restaurarContraseña(this.model.tipo_documento, this.model.numero)
          .then(_ => {
            this._navCtrl.push(CodigoPage, { user: this.model });
            this.toast.ShowMessage("Será enviado un código a tu correo o número de teléfono")
          })

      });
  }
}
