import { UserDataService } from './../../services/userData.service';
import { AlertsService } from './../../services/alerts.service';
import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController,App, Content} from 'ionic-angular';
import { ItemsService } from '../../services/items.service'
import{DetallesIncapacidadesPage} from '../detallesIncapacidades/detallesIncapacidades'
import {MainPage} from '../main/main'
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'page-incapacidadesPermanentes',
  templateUrl: 'incapacidadesPermanentes.html'

})
export class IncapacidadesPermanentesPage {
  static SCROLL_TRESHOLD = 100

  @ViewChild(Content) content: Content

  respuesta:any;
  incapacidadesPermanentes:any = null
  usuario:any;

  scrollIndicator: boolean = false

  constructor(
    public appCtrl: App,
    private loading:LoadingService,
    public navCtrl: NavController,
    public itemsService: ItemsService,
    private $zone: NgZone,
    private alerts:AlertsService,
    private userData:UserDataService) {

  }
  ionViewWillEnter(){
 
    this.realizarBusqueda();
  }

  _calcIndicatorState () {
    return (
      this.content.scrollTop + this.content.contentHeight <
      this.content.getContentDimensions().scrollHeight - IncapacidadesPermanentesPage.SCROLL_TRESHOLD
    )
  }

  toggleIndicator () {
    this.$zone.run(() => {
      this.scrollIndicator = this._calcIndicatorState()
    })
  }

  detalleIncapacidadPermanentes(detalle){
    this.appCtrl.getRootNav().push(DetallesIncapacidadesPage,{detalle:detalle});

  }
  realizarBusqueda(){
    this.loading.Normal('Cargando incapacidades...');
    this.usuario = this.userData.data.informacionPersonal;

    this.itemsService.incapacidadesPermanentes(this.usuario.numeroDocumento).then(responds =>{
      const data = JSON.parse(responds.data)
      this.loading.Hide();
      console.log(data);
      if(data.length>0){
      if(data[0].msjError == "NO EXISTEN IPP ASOCIADAS EN LOS ÚLTIMOS SEIS MESES" ||
      data[0].codError ==  403){
        this.alerts.OneButton("<h6>No tienes indemnizaciones por incapacidades permanentes</h6>","Continuar");
       }
      else{
        console.log(data[0])
        this.incapacidadesPermanentes = data;

        setTimeout(() => {
          this.scrollIndicator = this._calcIndicatorState()
        }, 2)
      }
    }

      if (!this.incapacidadesPermanentes) this.incapacidadesPermanentes = []
    })
    /*.catch(error => {
      this.loading.Hide();
       })*/
      
    }

    ionViewDidLoad(){
    this.navCtrl.insert(0,MainPage)
  }
  }
