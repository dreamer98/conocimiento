import { FingeprintService } from './../../services/fingerprint.service';
import env from '../../env';
import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController, MenuController, Events} from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
import { HomePage } from '../home/home';
import { listaMain } from './listaMain';
import { LoginPage } from '../login/login';
import { StoreService } from '../../services/store.service'
import { AuthenticationService } from '../../services/authentication.service'
import { LoadingService } from '../../services/loading.service';
import { AlertsService } from '../../services/alerts.service';
import { UserDataService } from '../../services/userData.service';

@Component({
  selector: 'page-main',
  templateUrl: 'main.html'

})
export class MainPage {
  rootPage;
  lista = new listaMain();

  @ViewChild('uploadPhoto') uploadPhoto: ElementRef

  user: any
  razonActiva:any= {
    razonSocial:null
  };

  profileOpened = false
  menuOpened = false

  menuIcon = 'menu'
  menuToggle = 'nav-menu'
  profileIcon = 'profile'
  profileToggle = 'profile-menu'
  eventOpenpage:any;
  askAgain:boolean=true;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public events: Events,
    private loading:LoadingService,
    private $fileTransfer: FileTransfer,
    private $file: File,
    private $fileOpener: FileOpener,
    private $store: StoreService,
    private authService: AuthenticationService,
    public $zone: NgZone,
    public fingerprintService:FingeprintService,
    public alerts:AlertsService,
    public userData:UserDataService
  ) {
    this.askAgain=JSON.parse(window.localStorage.getItem("fingerprintAsk"));
    this.rootPage = HomePage;
    this.lista.mostrarLista();
    const data = JSON.parse(localStorage.getItem("positiva-app:user"))
    const parametrosMenu = JSON.parse(JSON.parse(atob(data.split('.')[1])).user)
    console.log('USERRRRR:', parametrosMenu)

    this.user = parametrosMenu

    this.razonActual();

    //this.nombreUsuario = parametrosMenu.informacionPersonal.primerNombre + " " + parametrosMenu.informacionPersonal.primerApellido
  }


  ActivateFingerprint(){
    this.alerts.OneButton(`
    <img src="assets/imgs/fingerprint.svg" width="125">

    <h3 class="color1">Autenticación por huella</h3>
    <p>Se activo la autenticaciòn por huella por favor inicia nuevamente sesión.</p>`,

  _=>{
    window.localStorage.setItem("fingerprintAsk",JSON.stringify(true));
    this.logout();
  })
  }
  uploadProfilePhoto () {
    this.uploadPhoto.nativeElement.click()
  }

  openMenu () {
    this.menuOpened = true

    this.$zone.run(() => {
      this.menuIcon = 'home'
      this.profileIcon = 'arrow-forward'
      this.profileToggle = 'nav-menu'
    })
  }

  closeMenu () {
    this.menuOpened = false

    this.$zone.run(() => {
      this.menuIcon = 'menu'
      this.profileIcon = 'profile'
      this.profileToggle = 'profile-menu'
    })
  }

  openProfile () {
    this.profileOpened = true

    this.$zone.run(() => {
      this.profileIcon = 'home'
      this.menuIcon = 'arrow-back'
      this.menuToggle = 'profile-menu'
    })
  }

  closeProfile () {
    this.profileOpened = false

    this.$zone.run(() => {
      this.profileIcon = 'profile'
      this.menuIcon = 'menu'
      this.menuToggle = 'nav-menu'
    })
  }

  goToHome (type: string) {
    if (
      type === 'menu' && this.menuOpened ||
      type === 'profile' && this.profileOpened
    ) {
      this.rootPage = HomePage
    }
  }

  logout () {
    this.events.unsubscribe("OpenPage",this.eventOpenpage);
    this.authService.logout()
    this.$store.clear()

    this.navCtrl.setRoot(LoginPage)
  }

  abrirPagina(pagina) {
    if (pagina == LoginPage) {
     this.loading.Normal("Cargando...");

      // TODO: Check for "manejoDeDatos"
      this.logout()
    } else if (pagina == "MainPage") {
      this.navCtrl.insert(0, MainPage)
      this.navCtrl.setRoot(MainPage)
      this.menuCtrl.enable(false, "ionMenu")
    } else {
      this.rootPage = pagina
    }
  }

  ionViewDidLoad() {
    let pila = this.navCtrl.getViews();

    if (pila[0].component == LoginPage) {
      this.navCtrl.remove(0, 1)
    }
   this.eventOpenpage= this.events.subscribe("OpenPage",(page)=>{
     if(page=="CarnetPage"){
        this.viewCarnet();
     }else{
      this.abrirPagina(page);
     }

    });
  }



  viewCertificate() {
    this.loading.Normal('Generando certificado...');

    return this.openPDF('/certificados', 'certificado_positiva')
      .then(() => this.loading.Hide())
      .catch(error => {
        this.loading.Hide()

        console.log(error)
      })
  }

  viewCarnet () {
    const open = company => {
      this.loading.Normal('Generando carné...')

      this.openPDF(`/carnet/${company.tipoDocumento.code}/${company.numeroDocumento}/${company.phoneCode}`, 'carnet_positiva')
        .then(() => this.loading.Hide())
        .catch(error => {
          this.loading.Hide()

          console.log(error)
        })
    }

    if (this.userData.empleadoresActivos.length === 1) {
      open(this.userData.empleadoresActivos[0])
    } else {
      this.alerts.RelacionLaboral(this.userData.empleadoresActivos, open)
    }
  }

  openPDF (path: string, name: string) {
    const user = this.$store.get('user')
    const fileTransfer: FileTransferObject = this.$fileTransfer.create()

    return fileTransfer.download(
      env.API_ROOT + path,
      `${this.$file.dataDirectory}${name}.pdf`,
      true,
      { headers: { Authorization: user, } }
    )
    .then(entry => this.$fileOpener.open(entry.toURL(), 'application/pdf'))
  }


  razonActual(){

this.user.informacionPersonal.empleadores.forEach(element => {
if(element.activa){
this.razonActiva = element
}
else{
  this.razonActiva = {
    razonSocial:null
  };

}

});

if (!this.razonActiva)  {
  this.razonActiva = this.user.informacionPersonal.empleadores[0]
}
  }

}
