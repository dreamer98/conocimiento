//pages
import { RedUrgenciasPage } from './../red-urgencias/red-urgencias';
import { CitasRehabilitacionPage } from '../citas-rehabilitacion/citas-rehabilitacion';
import { IncapacidadesTemporalesPage } from '../incapacidadesTemporales/incapacidadesTemporales';
import { IncapacidadesPermanentesPage } from '../incapacidadesPermanentes/incapacidadesPermanentes';
import { NuevoPqrdPage } from '../nuevoPqrd/nuevoPqrd';
import { AsistenciaTelefonicaPage } from '../asistencia-telefonica/asistencia-telefonica';
import { InformacionInteresPage } from '../informacion-interes/informacion-interes';
import { PuntosAtencionPage } from '../puntos-atencion/puntos-atencion';
import { LoginPage } from '../login/login';
import { ConsultaAutorizacionMedicaPage } from '../consulta-autorizacion-medica/consulta-autorizacion-medica';
import { SolicitudAutorizacionMedicaPage } from '../solicitud-autorizacion-medica/solicitud-autorizacion-medica';



//Se maneja la lista del main, su contenido son los siguientes arrays
export class listaMain {

  manejoMain: Array<any> = [
    /*{
    id:1,
    icono:"ios-browsers",
    titulo:"Certificado afiliacion ARL",
    navegacion:CertificadoPage,
    permisos:false
  },*/
    /*{
      icono: "carnet.svg",
      titulo: "Carné afiliación ARL",
      html: "Carné<br>afiliación ARL",
      navegacion: CarnetPage,
      permisos: false
    },*/
    {

      icono: "urgency.svg",
      titulo: "Red de urgencias",
      html: "Red de<br>urgencias",
      navegacion: RedUrgenciasPage,
      permisos: false
    },
    {

      icono: "phone.svg",
      titulo: "Asistencia telefónica",
      html: "Asistencia<br>telefónica",
      navegacion: AsistenciaTelefonicaPage,
      permisos: false
    },
    {

      icono: "authorization-request.svg",
      titulo: "Solicitud de autorización médica",
      html: "Solicitud de<br>autorización médica",
      navegacion: SolicitudAutorizacionMedicaPage,
      permisos: false
    },
    {

      icono: 'authorization-consult.svg',
      titulo: 'Consulta de autorización médica',
      html: 'Consulta de<br>autorización médica',
      navegacion: ConsultaAutorizacionMedicaPage,
      permisos: false
    },
    {

      icono: "rehabilitation.svg",
      titulo: "Citas de rehabilitación",
      html: "Citas de<br>rehabilitación",
      navegacion: CitasRehabilitacionPage,
      permisos: false
    }, {

      icono: "temporal-incapacities.svg",
      titulo: "Incapacidades temporales",
      html: "Incapacidades<br>temporales",
      navegacion: IncapacidadesTemporalesPage,
      permisos: false
    }, {

      icono: "permanent-incapacities.svg",
      titulo: "Incapacidades permanentes",
      html: "Incapacidades<br>permanentes",
      navegacion: IncapacidadesPermanentesPage,
      permisos: false
    },
    {

      icono: "attention.svg",
      titulo: "Puntos de atención",
      html: "Puntos de<br>atención",
      navegacion: PuntosAtencionPage,
      permisos: false
    },
    {

      icono: "information.svg",
      titulo: "Información de interés",
      html: "Información de<br>interés",
      navegacion: InformacionInteresPage,
      permisos: false
    },
        {

         icono: "pqrd.svg",
         titulo: "Peticiones, quejas, reclamos y derechos de petición (PQRD)",
         html: "Peticiones, quejas,<br>reclamos y derechos de petición",
         navegacion: NuevoPqrdPage,
         permisos: false
       },
  ]


  //perfil por defec

  perfil: Array<any> = [
    // {
    //   icono: "md-settings",
    //   titulo: "Perfil",
    //   navegacion: PerfilPage,
    // }, {
    //   icono: "md-settings",
    //   titulo: "Configuraciones",
    // },
     {
      icono: "md-log-out",
      titulo: "  Cerrar sesión",
      navegacion: LoginPage

    },
  ]

  mostrarLista() {
    let manejar = this.manejoMain;

    // TODO: Check for an injected way of this list
    const data= JSON.parse(localStorage.getItem("positiva-app:user"))
    const parametrosMenu =  JSON.parse(JSON.parse(this.decodeBase64Unicode(data.split('.')[1])).user)
      //obtener permisos
    for (let i = 0; i < this.manejoMain.length; i++) {
      parametrosMenu.funcionalidades.forEach(function (item) {
       
        let listaTrue = manejar[i].titulo.toUpperCase().trim().localeCompare(item.codigo.replace(/_/g, " ").trim())
        //console.log(manejar[i].titulo.toUpperCase().trim() + '//'+ item.codigo.replace(/_/g, " ").trim())
        if (listaTrue == 0) {
            manejar[i].permisos = true;
      }
      })
      //cargar nombre usuario
    }
    const validarRelacionActiva = (JSON.parse(JSON.parse(atob(data.split('.')[1])).user)).informacionPersonal.empleadores.some(empleador => empleador.activa
    )
    //console.log(validarRelacionActiva)

    if (!validarRelacionActiva){
      manejar[0].permisos = false;
      manejar[2].permisos = false;
    }
    
    

  }


  decodeBase64Unicode (str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''))
  }
  

}
