import { UserDataService } from './../../services/userData.service';
import { AlertsService } from './../../services/alerts.service';
import { LoadingService } from './../../services/loading.service';
import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import {MainPage} from '../main/main';
import {ItemsService}  from '../../services/items.service'
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'page-citas-rehabilitacion',
  templateUrl: 'citas-rehabilitacion.html'

})
export class CitasRehabilitacionPage {

citasRehabilitacion:any = []
usuario:any;

  constructor(
    public itemsService : ItemsService,
    public navCtrl: NavController,
    private toast:ToastService,
    private loading:LoadingService,
    private alerts:AlertsService,
    private userData:UserDataService) {



}
ionViewDidLoad(){
  this.navCtrl.insert(0,MainPage)
  }
ionViewWillEnter(){
  this.loading.Normal("Cargando...");
  this.usuario = this.userData.data.informacionPersonal;
   console.log(this.usuario)
   this.itemsService.citasRehabilitacion().then(responds =>  {
     this.loading.Hide();
     const data = responds.data;
     if(data===undefined || data.length == 0){

      this.citasRehabilitacion = null;
   }
 else {
  this.citasRehabilitacion = data;

 }
   })

}

enviar(citasRehabilitacionId){
 this.loading.Normal("Cargando...");


  this.itemsService.confirmarCita(citasRehabilitacionId).then(respuesta => {
   const data = JSON.parse(respuesta.data)


  let  primerSplit = data.split(">")
  let  segundoSplit = primerSplit[5].split("<")

    if(segundoSplit[0] == "true"){
   this.loading.Hide();
   this.alerts.OneButton("<h1 class='color1'>Confirmar cita</h1><p>Tu cita ha sido confirmada</p>","OK");

}
else{
  this.toast.ShowMessage("Tu cita ya se ha confirmado o ha sido reasignada")
}

  })
  /*.catch( _ => {
    this.loading.Hide();
    this.toast.ShowMessage("Existen problemas con el servicio. Por favor, inténtelo más tarde")
  })*/


}





cancelar(citasRehabilitacionId){


    this.alerts.TowButtons(
      `<img src="assets/icon/warning.svg" width="125">    
      <h3 class="color1">Cancelación de cita</h3>
      <p>¿Estás seguro de que deseas cancelar esta cita?</p>`,
    "Si",
    "No",
    _=>{
    this.loading.Normal("Cargando...");
    this.itemsService.cancelarCita(citasRehabilitacionId).
    then( respuesta =>{
    let  primerSplit = respuesta.split(">")
    let  segundoSplit = primerSplit[5].split("<")
    console.log(segundoSplit)
    console.log(segundoSplit[0])
      if(segundoSplit[0] == "true"){

this.alerts.OneButton("<h1>Cancelación de cita</h1><p>Su cita ha sido cancelada satisfactoriamente </p>");
this.loading.Hide();


}
else{
  this.toast.ShowMessage("La cita ya está confirmada o ha sido reasignada")
}
    })


  });



}



}
