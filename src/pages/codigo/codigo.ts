
import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { AuthenticationService } from '../../services/authentication.service'
import {LoginPage} from '../login/login'
import { LoadingService } from '../../services/loading.service';
import { ToastService } from '../../services/toast.service';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AlertsService } from '../../services/alerts.service';

@Component({
  selector: 'page-codigo',
  templateUrl: 'codigo.html'

})
export class CodigoPage {
  user;
  codigo;
  clave;
  claveRepetida;
  codeForm:FormGroup;

  constructor( 
    public navCtrl: NavController,
    public navParams: NavParams,
    public _authService:AuthenticationService,
    public loading:LoadingService,
    public toast:ToastService,
    private formBuilder: FormBuilder,
    private alerts:AlertsService
  ) {
    this.user = navParams.get('user');
    this.codeForm = this.formBuilder.group({
      codigoInput: ['',Validators.compose([ 
        Validators.required,
      Validators.minLength(6),
      Validators.maxLength(6)
    ])],
    claveRepetidaInput: ['',Validators.compose([ 
      Validators.required,
    Validators.minLength(6)
  ])],
   claveInput: ['',Validators.compose([ 
      Validators.required,
    Validators.minLength(6)
  ])]
 
    });
  }


verificarCodigo(){
  this.loading.Normal("Cargando");
  
  if(this.clave == this.claveRepetida){
     this._authService.activacionCuenta(this.user,this.codigo,this.clave).then(respuesta => {
     this.toast.ShowMessage("Se cambió satisfactoriamente tu contraseña")
           this.navCtrl.push(LoginPage);

    })
    .catch(error => {
      if (error instanceof SyntaxError) {
        this.toast.ShowMessage ("Existen problemas. Por favor, inténtalo nuevamente");
      }
      if(error.status == 400){
        this.toast.ShowMessage ("Código inválido");
      }
      else if(error.status == 409){
        this.toast.ShowMessage ("Tu código ha expirado. Por favor, solicita una restauración de código");

      }

    
    })
  
  }
  else {
    this.toast.ShowMessage ("Las contraseñas no coinciden ");
  }
  this.loading.Hide(1);
}

recuperarClave(){
  this.alerts.OneButton(` <img src="assets/icon/warning.svg" width="125">
  <h3 class="color1">Recuperar contraseña</h3>
  <p>Se ha enviado un correo y/o mensaje de texto a los datos registrados con el usuario: </p>
  <strong class="color1">${this.user.numero}</strong>
`,"Enviar",()=>{
  this.loading.Normal("Cargando...");
  this._authService.restaurarContraseña(this.user.tipo_documento,this.user.numero).then(_ => {
  
   this.toast.ShowError("Será enviado un código a tu correo y/o número de teléfono");
   this.loading.Hide();
    })
   
    
});


  }

  goBack() {
    this.navCtrl.pop()
  }

}
