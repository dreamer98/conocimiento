import { UserDataService } from './../../services/userData.service';
import { HomePage } from './../home/home';
import { Component,  ViewChild, ElementRef } from '@angular/core';
import { NavController,  Events } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';


import { ItemsService } from '../../services/items.service'
import { MainPage } from '../main/main'
import { GoogleMapsProvider } from '../../services/google-maps.service';
import { AlertsService } from '../../services/alerts.service';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'page-red-urgencias',
  templateUrl: 'red-urgencias.html'

})
export class RedUrgenciasPage {
  @ViewChild('slide') slides: Slides;
  @ViewChild('redMap') private mapElement: ElementRef;
  direccion: any;
  ciudad: any;
  departamentos: any;
  seleccionDepartamento: any;
  municipios: any;
  seleccionMunicipios: any;
  descripcion: any;
  redComplemento: any;
  numeroDeTelefonos: any;
  relacionesLaborales = [];
  valorRelacion: any;
  telefonosFinalRamas=[];
  arrayDescripcion = [];
  ubicacionDistancia: any[];
  habilitarBusqueda:any;
  loadingPlaces:number;
  placesFinish:boolean=false;
  empleadoresValidator:boolean=false;
  constructor(private navCtrl:NavController,
    public geolocation: Geolocation,
    private itemsService: ItemsService,
    private maps:GoogleMapsProvider,
    private alerts:AlertsService,
    private events:Events,
    private userData:UserDataService
  ) {
    this.loadingPlaces=0;
    this.redComplemento = "listaDeLugares";
    this.descripcion = []



  }






  ionViewDidLoad() {


    this.loadMap();

    this.navCtrl.insert(0, MainPage);
  }

  loadMap(){
    this.loadingPlaces=0;
    this.maps.loadMap(this.mapElement.nativeElement,didLoad=>{
      if(didLoad){
        console.log("Mapa cargo.");

          this.direccionUsuario();
          if (this.userData.empleadoresActivos.length <= 1) {
              this.loadPhoneNumbers(this.userData.empleadoresActivos[0].phoneCode);
           }





      }else{
        this.loadingPlaces=2;
        this.alerts.OneButton("No se pudo  cargar el mapa","Recargar",()=>{
          this.loadMap();
        });

      }

      });
  }

  ValidateNumbers(){
    if(this.userData.empleadoresActivos.length > 1){
      this.alerts.RelacionLaboral(this.userData.empleadoresActivos,
        relacion=>{

              this.loadPhoneNumbers(relacion.phoneCode);
        });
    }
  }

  loadPhoneNumbers(phoneCode:string){
    this.itemsService.telefonos(phoneCode)
    .then(responds => {
      this.telefonosFinalRamas = responds.data.numero.split(";")
    })
  }




  Swipe(hasNext:boolean){
    if(hasNext){
      this.slides.slideNext();
    }else{
      this.slides.slidePrev();
    }
  }

  placesLoaded(last:any){
   if(last){this.placesFinish=true;}
  }

  direccionUsuario() {


    this.loadingPlaces=0;
    this.placesFinish=false;
    this.itemsService.redAsistencia(this.maps.coordenates.coords)

  .then(respuesta => {
        if(respuesta!==null){


          console.log(respuesta.data);
          if(respuesta.data.length>0){


      this.maps.PushMedicineRedMarker(respuesta.data,(points)=>{
        this.loadingPlaces=1;



          this.descripcion = points;
          console.log(this.descripcion);

         });















    }else{
      console.log("No se pudo cargar los puntos de atención");
    }





    }else{
      this.alerts.OneButton("No existe red de urgencias cercana a tu ubicación","Aceptar",()=>{
        this.events.publish("OpenPage",HomePage);
      });
    }
    })


  }
}
