import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ItemsService } from '../../services/items.service'
import { MainPage } from '../main/main'
import { GoogleMapsProvider } from '../../services/google-maps.service';
import { LoadingService } from '../../services/loading.service';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'page-puntos-atencion',
  templateUrl: 'puntos-atencion.html'

})
export class PuntosAtencionPage {
  @ViewChild('slide') slides: Slides;
  departamentos: any;
  municipios: any;
  procedimiento:any;
  descripcion: any;
  atencionComplemento: any;
  seleccionDepartamento: any = ''
  seleccionMunicipios: any = ''
  seleccionPuntosAtencion: any = '';
  loadingPlaces:number;

  @ViewChild('atencionMap') private mapElement: ElementRef
  mapLoaded:boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public geolocation: Geolocation,
    private $itemsService: ItemsService,
    private maps:GoogleMapsProvider,
    private loading:LoadingService
  ) {
    this.atencionComplemento = "listaDeLugares";
    this.mapLoaded=false;
    this.loadingPlaces=0;
  }

 
  ionViewDidLoad() {



    this.loadMap();
    this.navCtrl.insert(0, MainPage)
  }

  loadMap(){
    this.loadingPlaces=0;
    this.maps.loadMap(this.mapElement.nativeElement,didLoad=>{
      if(didLoad){
        this.mapLoaded=true;
        this.loadingPlaces=1;
      }else{
        this.loadingPlaces=2;

      }
     });
     this.$itemsService.departamentos().then(responds => {
      this.departamentos = responds.data;
      console.log(this.departamentos)
    })
 
  }



 Swipe(hasNext:boolean){
  if(hasNext){
    this.slides.slideNext();
  }else{
    this.slides.slidePrev();
  }
}


direccionUsuario() {

  if(this.seleccionDepartamento!=="" && this.seleccionMunicipios!=="" &&
  this.seleccionPuntosAtencion!==""){
  this.loading.Normal("Cargando Puntos...");

   this.$itemsService.vistaPuntosAtencion(this.seleccionMunicipios, this.seleccionPuntosAtencion)
     .then(respuesta => {
       this.loading.Hide();
       let puntos = respuesta.data;
    

        this.maps.PushMedicineAtencionMarker(puntos,points=>{
          this.descripcion=points;
        });

        if (puntos.length === 1) {
          this.maps.newCenter(puntos[0].latitud, puntos[0].longitud, center => {
            this.maps.map.panTo(center)
            this.maps.map.setZoom(16);
          })
        }
     })

 }else{

 }
}



  cambiarMarcadores() {
    this.$itemsService.municipios(this.seleccionDepartamento).then(respuesta => {

      this.municipios = respuesta.data;

    })
  
  }


  cambiarTramites(){

    this.$itemsService.prodecimiento().then(respuesta => {

      this.procedimiento = respuesta.data;

    })

  }


  getFabButton(fab) {
    fab.close()
  }
}
