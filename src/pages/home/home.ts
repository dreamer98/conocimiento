import { UserDataService } from './../../services/userData.service';
import { Component } from '@angular/core'
import { NavController,Events} from 'ionic-angular'
import { MainPage } from "../main/main";
import { listaMain } from '../main/listaMain';
import {RedUrgenciasPage} from '../red-urgencias/red-urgencias';

import { LoadingService } from '../../services/loading.service';
import { ConsultaAutorizacionMedicaPage } from '../consulta-autorizacion-medica/consulta-autorizacion-medica';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  lista = new listaMain()

  userName: string
  carnetActive: boolean

  quickAccess = {
    carnet: "CarnetPage",
    consulta: ConsultaAutorizacionMedicaPage,
    red:RedUrgenciasPage
  }

  constructor(
    public navCtrl: NavController,
    private loading:LoadingService,
    private userData:UserDataService,
    private events:Events) {
      }

  abrirPagina(pagina) {
   
      this.events.publish("OpenPage",pagina);
  
      

  }

  ionViewDidLoad() {
    this.loading.Hide();
    this.navCtrl.insert(0, MainPage)
    this.lista.mostrarLista()
    const name = this.userData.data.informacionPersonal.primerNombre;
    this.userName = name.charAt(0) + name.slice(1).toLowerCase()
    this.carnetActive = this.userData.carnetActivo;

  }

 
}
