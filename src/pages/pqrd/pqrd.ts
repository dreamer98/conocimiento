import { Component } from '@angular/core';
import { App} from 'ionic-angular';


//pages
 
 import {NuevoPqrdPage} from '../nuevoPqrd/nuevoPqrd';
@Component({
  selector: 'page-pqrd',
  templateUrl: 'pqrd.html'

})
export class PqrdPage {

  constructor(  public appCtrl: App) {

}

generarNuevoPqrd(){
   this.appCtrl.getRootNav().push(NuevoPqrdPage);
}
}
