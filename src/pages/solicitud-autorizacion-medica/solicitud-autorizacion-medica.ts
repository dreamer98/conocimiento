import { Component } from '@angular/core'
import { ModalController } from 'ionic-angular'
import { ConsultService } from '../../services/consult.service'
import { NewRequestPage } from '../../pages/new-request/new-request';
import { StoreService } from '../../services/store.service';
import { LoadingService } from '../../services/loading.service';


@Component({
  selector: 'page-solicitud-autorizacion-medica',
  templateUrl: 'solicitud-autorizacion-medica.html',
})
export class SolicitudAutorizacionMedicaPage   {
  sinisters=null
  usuario:any;

  private _initialFetch = false

  constructor(
    private $modalCtrl: ModalController,
    private $consultService: ConsultService,
  private $store: StoreService,
  private loading:LoadingService) {
    

 
  }

    private _setSinisters() {
      this.loading.Normal('Cargando siniestros...');

      this.$consultService.getSinisters()
        .then(sinisters => {
          this.loading.Hide()
          this.sinisters = (sinisters as any).data || [];
          this.sinisters.sort((a,b)=>{
            return b.fechaHoraSiniestro - a.fechaHoraSiniestro
      });
        })
   
    }

   

    newRequest(sinister) {
      this.$modalCtrl.create(NewRequestPage, { sinister }).present()
    }

    updateSinisters(company) {
      if (this._initialFetch) {
        this._setSinisters()
      }
    }
    ionViewDidLoad(){
      this.usuario = this.$store.getParse('user')
      this._setSinisters()
        this._initialFetch = true
    //this.$navCtrl.insert(0,MainPage)
  }
  }
