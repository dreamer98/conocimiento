import { UserDataService } from './../../services/userData.service';
import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular'
import {MainPage} from "../main/main"



@Component({
  selector: 'page-detallesIncapacidades',
  templateUrl: 'detallesIncapacidades.html'

})
export class DetallesIncapacidadesPage {

  incapacidades : any = null;
  documento:any;
  datosPago:any;
  incapacidadesPermanentes:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userData:UserDataService) {

    }

ionViewDidLoad(){
this.navCtrl.insert(0,MainPage);
this.incapacidades = this.navParams.get("incapacidadesTemporales")
    this.incapacidadesPermanentes = this.navParams.get("detalle")

    this.documento = this.userData.data.informacionPersonal.numeroDocumento;

    if(this.incapacidades != null){
      if(this.incapacidades.datosPago.tipo.descripcion == "C"){
        this.datosPago = "Cobro directo";
      }
      else{
        this.datosPago = "Descuento autoloquidación"
      }
    }
    else {
      this.incapacidades = null;
    }




}

}
