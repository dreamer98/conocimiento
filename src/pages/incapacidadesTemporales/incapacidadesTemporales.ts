import { UserDataService } from './../../services/userData.service';
import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController,MenuController, App, Content } from 'ionic-angular';
import { ItemsService } from '../../services/items.service'
import {DetallesIncapacidadesPage} from "../detallesIncapacidades/detallesIncapacidades"
import {MainPage} from "../main/main"
import { LoadingService } from '../../services/loading.service';
@Component({
  selector: 'page-incapacidadesTemporales',
  templateUrl: 'incapacidadesTemporales.html'

})
export class IncapacidadesTemporalesPage {
  static SCROLL_TRESHOLD = 100

  @ViewChild(Content) content: Content


  incapacidadesTemporales:any = null
  usuario:any;

  scrollIndicator: boolean = false

  constructor(
    public navCtrl: NavController,
    public appCtrl:App ,
    public menuCtrl: MenuController,
    public itemsService: ItemsService,
    private $zone: NgZone,
    private userData:UserDataService,
    private loading:LoadingService) {
   
// 1096235487
  }
  detalleIncapacidadTemporal(incapacidadesTemporales){
    this.appCtrl.getRootNav().push(DetallesIncapacidadesPage,{incapacidadesTemporales:incapacidadesTemporales});
  }


  ionViewWillEnter(){
    this.menuCtrl.enable(true,'ionMenu');
    this.navCtrl.insert(0,MainPage);
    this.loadIncapacidadesTemporales();
  }

  loadIncapacidadesTemporales(){
   
    this.usuario = this.userData.data.informacionPersonal;
 
    
     this.loading.Normal('Cargando incapacidades...');
 
     this.itemsService.incapacidadesTemporales(this.usuario.numeroDocumento).then( respuesta => {
      if(respuesta.data!==undefined){
        const data = JSON.parse(respuesta.data)
        console.log(data)
        this.incapacidadesTemporales = data.incapacidadesTemporales || []
  
      
          this.scrollIndicator = this._calcIndicatorState()
          this.loading.Hide(1);
      }else{
        this.incapacidadesTemporales =[];
        this.loading.Hide();
        
      }
      
      
     })
  }

  _calcIndicatorState () {
    return (
      this.content.scrollTop + this.content.contentHeight <
      this.content.getContentDimensions().scrollHeight - IncapacidadesTemporalesPage.SCROLL_TRESHOLD
    )
  }

  toggleIndicator () {
    this.$zone.run(() => {
      this.scrollIndicator = this._calcIndicatorState()
    })
  }
}
