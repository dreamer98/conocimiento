import { Component } from '@angular/core'
import { NavParams ,ModalController, ViewController } from 'ionic-angular'
import { SurveyPage } from '../../pages/survey/survey'
import { ConsultService } from '../../services/consult.service'
import { AlertsService } from '../../services/alerts.service';

@Component({
  selector: 'page-evaluate-services',
  templateUrl: 'evaluate-services.html',
})
export class EvaluateServicesPage {
  data: any = {}

  constructor(
    $navParams: NavParams,
    private $modalCtrl: ModalController,
    private $viewCtrl: ViewController,
    private alerts:AlertsService,
    private $consultService: ConsultService) {
    this.data = $navParams.get('request')
  }

  evaluate(service, takeSurvey) {
    if(takeSurvey){
      return this.takeSurvey(service)
    }else{
      this.alerts.OneButton(`
      <img src="assets/icon/warning.svg" width="125">
      <h3 class="color1">Apreciado(a) asegurado</h3>
      <p>Aún no has tomado el servicio. Una vez lo tomes no olvides evaluarlo</p>
    `);
    this.$viewCtrl.dismiss(false);
    }
    
/*
    this.$consultService
      .evaluate({
        codigoServicio: service.codigoServicio,
        numeroSolicitud: this.data.numeroSolicitud,
        efectuado: takeSurvey
      })
      .then(_ => {
        if (takeSurvey) {
          this.takeSurvey(service)
        } else {
          this.alerts.OneButton(`
            <img src="assets/icon/warning.svg" width="125">
            <h3 class="color1">Apreciado(a) asegurado</h3>
            <p>Aún no has tomado el servicio. Una vez lo tomes no olvides evaluarlo</p>
          `)


        }
      })
*/
  }

  takeSurvey(service) {
   let surveyModal= this.$modalCtrl.create(SurveyPage, {
      params: {
        supplier: service.movimientos[0].nombreProveedor,
        service_id: service.id,
        service_code: service.codigoServicio,
        service_name: service.descripcion,
        request_number: this.data.numeroSolicitud,
        authorization_rejection_number: service.movimientos[0] ? service.movimientos[0].numeroAutorizacionNegacion : 'N/A'
      }
    });
    surveyModal.onDidDismiss(didAnswer => {
        if(didAnswer!=undefined){
             this.dismiss(didAnswer);
        }
    });

    surveyModal.present()
  }

  dismiss(needToReload:boolean) {
    this.$viewCtrl.dismiss(needToReload);
  }
}
