import { UserDataService } from './../../services/userData.service';
import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import {ItemsService} from '../../services/items.service'
import {MainPage} from '../main/main'
import { AlertsService } from '../../services/alerts.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'page-nuevoPqrd',
  templateUrl: 'nuevoPqrd.html'

})
export class NuevoPqrdPage {

  mensaje:any;
 
  constructor(
    public navCtrl: NavController,
    public itemsService: ItemsService,
    private userData:UserDataService,
    private alert:AlertsService,
    private loading:LoadingService) {
 
  }

 


enviarPqrd(){
 this.loading.Normal("Enviando PQRD...");
  this.itemsService.enviarPqrd(this.mensaje).then ( _ => {
    this.loading.Hide();
    this.alert.OneButton(`<img src="assets/icon/success.svg" width="125">
    <h3 class="color1">Solicitud exítosa</h3>
    <p>Se envió correctamente el mensaje. Pronto tendrás respuesta al correo
    <br>
    <br>
    <strong class="color1" >${this.userData.data.informacionPersonal.correoElectronico}</strong> </p>`)
    this.mensaje="";
  })
 


}
ionViewDidLoad(){
  this.navCtrl.insert(0,MainPage)
}
}
