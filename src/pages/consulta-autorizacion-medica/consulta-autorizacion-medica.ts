import { UserDataService } from './../../services/userData.service';
import { Component, ViewChild, NgZone } from '@angular/core'
import { NavController, ModalController, Content } from 'ionic-angular'

import { ConsultServicesModalComponent } from '../../components/consult-services-modal/consult-services-modal'
import { EvaluateServicesPage } from '../../pages/evaluate-services/evaluate-services'
import {MainPage} from '../main/main'

import { ConsultService } from '../../services/consult.service'
import { LoggerService } from '../../services/logger.service'


import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'page-consulta-autorizacion-medica',
  templateUrl: 'consulta-autorizacion-medica.html',
})
export class ConsultaAutorizacionMedicaPage {
  static SCROLL_TRESHOLD = 100

  @ViewChild(Content) content: Content

  requests: any[] = null
  usuario:any;

  scrollIndicator: boolean = false

  constructor(
    private $modalCtrl: ModalController,
    private loading:LoadingService,
    private $consultService: ConsultService,
    private $logger: LoggerService,
    private $navCtrl:NavController,
    private $zone: NgZone,
    private userData: UserDataService) {

    }

  _loadData () {

    this.loading.Normal('Cargando solicitudes...');

    this.requests=null;
    this.$consultService.getRequests()
      .then((requests: any) => {
        this.requests = requests.data || [];
        this.requests.map(request=>{
          let temp=typeof request.fechaSolicitud;
          if( temp == "number"){
            request.fechaSolicitud = new Date(Number(request.fechaSolicitud));
          }else if(temp == "string"){
            request.fechaSolicitud = new Date(request.fechaSolicitud);
          }
        });
        this.requests.sort((a,b)=>{
          return b.fechaSolicitud - a.fechaSolicitud
          });


          this.scrollIndicator = this._calcIndicatorState()




        this.loading.Hide(0.5);
      })

  }

  _calcIndicatorState () {
    return (
      this.content.scrollTop + this.content.contentHeight <
      this.content.getContentDimensions().scrollHeight - ConsultaAutorizacionMedicaPage.SCROLL_TRESHOLD
    )
  }

  toggleIndicator () {
    this.$zone.run(() => {
      this.scrollIndicator = this._calcIndicatorState()
    })
  }

  openServices(request) {
    this.$logger.log('Opening services from request:', request)

    this.$modalCtrl
      .create(ConsultServicesModalComponent, { request })
      .present()
  }

  evaluateServices(request) {
    this.$logger.log('Triying to evaluate request:', request)

   let evaluateModal= this.$modalCtrl
      .create(EvaluateServicesPage, { request })
      evaluateModal.onDidDismiss(needToReload=>{
       console.log("Need to reload ",needToReload);
        if(needToReload){

          this._loadData();
        }

      });
      evaluateModal.present();
  }



  ionViewDidEnter () {
    this.usuario = this.userData.data.informacionPersonal;
    this._loadData()
  }

  ionViewDidLoad(){
  this.$navCtrl.insert(0,MainPage)
}
}
