
export class formularios {
  razonSocial : Array<any> = [{
    razonSocial:'SERVICIOS Y PRODUCTOS DE OCCIDENTE',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900760640'
  },
  {
    razonSocial:'FORTOX SA',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'860046201'
  },
  {
    razonSocial:'CARBONES DEL CERREJON LIMITED',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'860069804'

  },
  {
    razonSocial:'CLINICA LAURA DANIELA SA',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900008328'
  },
  {
    razonSocial: 'LABORAMOS CEL LTDA',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900072565'
  },
  {
    razonSocial:'EST ASERDIR SAS',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900140483'

  },
  {
    razonSocial:'MEDICALL TALENTO HUMANO SAS',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900682543'
  },
  {
    razonSocial:'SINERGIA GLOBAL EN SALUD SAS',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900363673'
  },
  {
    razonSocial: 'AMPARO ROJAS VELA',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'40011663'
  },
  {
    razonSocial:'CONSORCIO DRAGADOS CONCAY',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'900493568'
  },
  {
    razonSocial:'SENADO DE LA REPUBLICA',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:'899999103'
  },
  {
    razonSocial:'QUIROGA ISIDRO',
    tipoDocumento:'NUMERO_IDENTIFICACION_TRIBUTARIA',
    numeroDocumento:''
  }
]
}
