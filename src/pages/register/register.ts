import { AlertsService } from './../../services/alerts.service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular'
import { AuthenticationService } from '../../services/authentication.service'
import {formularios} from './formularios'
import {CodigoPage} from '../codigo/codigo'
import { LoadingService } from '../../services/loading.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user:any;
  responds:any;
  formulario = new formularios();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    public _authService: AuthenticationService,
    public loading:LoadingService, 
    private toast:ToastService,
    private alerts:AlertsService ) {
    this.user = navParams.get('user');
    this.responds = navParams.get('responds');
    // Random de empresas

    let random

    for (let i = 0; i < 5; i++) {
      random = Math.floor(Math.random() * this.formulario.razonSocial.length)
      this.formulario.razonSocial.splice(random, 1)
    }

    const relacionLaboral = this.responds.relacionLaboral
    this.formulario.razonSocial = this.formulario.razonSocial.filter(company => company.razonSocial !== relacionLaboral.razonSocial)
    this.formulario.razonSocial[random] = relacionLaboral
  }

  goBack() {
    this.navCtrl.pop()
  }

  // manejo con webService
  registrar ()  {
  this.loading.Normal("Cargando..",1);
  for(let a = 0; a < this.formulario.razonSocial.length; a++ ){

    if(this.user.razonSocial.trim() == this.formulario.razonSocial[a].razonSocial.trim()){
      this.user.tipoDocumentoRazonSocial = this.formulario.razonSocial[a].tipoDocumento
      this.user.numeroDocumentoRazonSocial = this.formulario.razonSocial[a].numeroDocumento
      break;

    }
  }

  this.alerts.register((response:boolean)=>{
    if(response){
      let tipoDocumento ="";
    switch(this.user.tipo_documento){
      case "CC":
      tipoDocumento="CEDULA_DE_CIUDADANIA";
      break;
      case "CE":
      tipoDocumento="CEDULA_DE_EXTRANJERIA";
      break;
      case "PA":
      tipoDocumento="PASAPORTE";
      break;
      case "RC":
      tipoDocumento="REGISTRO_CIVIL";
      break;
      case "TI":
      tipoDocumento="TARJETA_DE_IDENTIDAD";
      break;
      case "SI":
      tipoDocumento="SIN_IDENTIFICACION";
      break;
      case "PE":
      tipoDocumento="PERMISO_ESPECIAL";
      break;
      

    }
    
  this.loading.Normal("Registro en proceso. Esto puede tardar unos minutos...");
this._authService.register(this.user,tipoDocumento).then(data => {

if(data.success){
  this.loading.Hide();
  this.toast.ShowError(`Usuario registrado correctamente. Será enviado un código
   a tu correo ${this.user.correo} y/o mensaje de texto al celular.`)
  this.navCtrl.push(CodigoPage,{user:this.user})
}
else {
  throw data.mesagge;
}
}).catch(error => {
console.error(error);
this.loading.Hide();
if(error.status == 400){
  this.toast.ShowError(error.error.error,this.toast.position.BOTTOM);

}

})
  }
  else{
    this.toast.ShowMessage("No has aceptado las condiciones de uso.")
  }

  });
   


}

}
