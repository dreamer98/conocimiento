import { UserDataService } from './../../services/userData.service';
import { AlertsService } from './../../services/alerts.service';
import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import {MainPage} from '../main/main'
import { ItemsService } from '../../services/items.service'
import { LoadingService } from '../../services/loading.service';


@Component({
  selector: 'page-asistencia-telefonica',
  templateUrl: 'asistencia-telefonica.html'

})
export class AsistenciaTelefonicaPage {

  tels: any[]
 
  constructor(
    private navCtrl: NavController,
    private itemsService: ItemsService,
    private loading:LoadingService,
    private alerts:AlertsService,
    private userData:UserDataService
  ) {



    }

    ionViewWillEnter(){
      this.loadAsistenceNumbers();
      this.navCtrl.insert(0,MainPage)
    }



  loadAsistenceNumbers(){
   
  
    if(this.userData.empleadoresActivos.length<=1){
      
    this.loadPhoneNumbers(this.userData.empleadoresActivos[0].phoneCode);
  }else{
    this.alerts.RelacionLaboral(this.userData.empleadoresActivos,
    relacion=>{
        this.loadPhoneNumbers(relacion.phoneCode);
    });
  }
 
  }


loadPhoneNumbers(phoneCode:string){
  this.loading.Normal("Cargando");
  this.itemsService.telefonos(phoneCode).then(responds=> {
    this.loading.Hide(1);
   
      this.tels = formatTels(responds.data.numero.split(";"));
    
   
   
  })
}
}

function formatTels (tels) {
  return tels.map(tel => {
    const isLocal = tel.startsWith('03')

    return {
      call: isLocal ? `031 ${tel.slice(3)}` : tel,
      format: isLocal ? `(031) ${tel.slice(3)}` : tel,
      number: `${isLocal ? '0' : ''}${tel}`,
      reference: /^\(?0?31?\)?/.test(tel) || isLocal ? 'Bogotá':'Resto del país'
    }
  })
}

