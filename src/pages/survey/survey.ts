import { Component } from '@angular/core'
import { NavParams, ViewController } from 'ionic-angular'
import { SurveyService, Survey, Answer } from '../../services/survey.service'
import { StoreService } from '../../services/store.service'
import { LoadingService } from '../../services/loading.service';
import { AlertsService } from '../../services/alerts.service';

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {
  ICON_FORMAT_RE = /^ion-|-outline$/g

  params: any = {
    /*service_name: 'Consulta primera vez',
    supplier: 'Compensar EPS'*/
  }

  survey: Survey = {} as any
  iconsMap = {
    Malo: 'sad',
    Regular: 'happy',
    Bueno: 'great'
  }

  constructor(
    $navParams: NavParams,
    private loading:LoadingService,
    private $surveyService: SurveyService,
    private $store: StoreService,
    public $viewCtrl: ViewController,
    private alerts: AlertsService) {
    this.params = $navParams.get('params')
    console.log(this.params);
  }

  ngOnInit() {
    this.loading.Normal('Cargando encuesta...');

    this.$surveyService.get()
      .then(survey => {
        SurveyPage.sortAnswers(survey)
        this.survey = survey
        this.loading.Hide()
      })
      .catch(() => {
        this.loading.Hide()
      })
     /* .catch(_ => {
        // TODO: Handle rejection
        this.loading.Hide()
      })*/
  }

  static sortAnswers (survey) {
    const options = survey.preguntas[0].opcionesRespuesta

    survey.preguntas[0].opcionesRespuesta = [
      options.find(option => option.descripcion === 'Bueno'),
      options.find(option => option.descripcion === 'Regular'),
      options.find(option => option.descripcion === 'Malo')
    ]
  }

  private _getAnswersAndVerify() {
    const answered = []

    for (const question of this.survey.preguntas) {
      const answers: Answer[] = question.opcionesRespuesta.filter(
        (option: any) => option.selected && delete option.selected)

      if (!answers.length) {
        this.alerts.OneButton("<h4><strong>Apreciado(a) asegurado(a)</strong></h4><br>No has respondido a todas las preguntas. Por favor, verifica")
        return
      }

      answered.push({
        id: question.id,
        comentario: '',
        opcionesRespuesta: answers
      })
    }

    console.log('Answered.sas.fasfa', answered)

    return answered
  }

  applySurvey() {
    const { informacionPersonal }= this.$store.getParse('user')

    this.loading.Normal('Guardando encuesta...');

    this.$surveyService.apply(this.survey.id, {
      servicio: {
        servicioCuidaId: this.params.service_id,
        codigo: this.params.service_code,
        descripcion: this.params.service_name
      },
      preguntas: this._getAnswersAndVerify()
    })
      .then(_ => {
        this.loading.Hide(0, () => {
          this.alerts.OneButton(`
            <img src="assets/icon/success.svg" width="125">
            <h3 class="color1">Gracias por tu evaluación</h3>
          `,"Aceptar",()=>this.$viewCtrl.dismiss(true),false);
        })

      })
      .catch(_ => {
        // TODO: Handle rejection
        this.$viewCtrl.dismiss(false);
      })
  }

  setAnswer(option, options) {
    if (options.prevSelected) {
      options.prevSelected.selected = false
    }

    (options.prevSelected = option).selected = true
  }
}
