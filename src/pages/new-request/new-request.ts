import { AlertsService } from './../../services/alerts.service';
import { Component, ElementRef, ViewChild } from '@angular/core'
import { NavParams, ViewController } from 'ionic-angular'
import { ConsultService } from '../../services/consult.service'
import { StoreService } from '../../services/store.service'
import { CameraService } from '../../services/camera.service';
import { ToastService } from '../../services/toast.service';
import { LoadingService } from '../../services/loading.service';


@Component({
  selector: 'page-new-request',
  templateUrl: 'new-request.html',
})
export class NewRequestPage {
  static FILE_MAX_SIZE = 5242880
  static FILE_EXT_RE = /\.(?:pdf|jpg)$/i

  @ViewChild('inputFiles') inputFiles: ElementRef

  data: any
  files: File[] = []
  maxFiles = 5
  file:string;
  fileUID = 0

  constructor(
    $navParams: NavParams,
    private toast:ToastService,
    private $viewCtrl: ViewController,
    private loading:LoadingService,
    private $consultService: ConsultService,
    private $store: StoreService,
    private camera:CameraService,
    private alerts:AlertsService
  ) {
    this.data = $navParams.get('sinister')
  }

  private _getCorrectFiles(files: File[]) {
    return Array.prototype.filter.call(files, file => {
      const isDuplicated = this.files.some(_file => {
        return (
          file.name === _file.name &&
          file.size === _file.size
        )
      })

      return !isDuplicated && (
        // Check allowed type
        NewRequestPage.FILE_EXT_RE.test(file.name) &&

        // Check allowed max file size
        file.size <= NewRequestPage.FILE_MAX_SIZE
      )
    })
  }

  private _showAlert(title: string, subTitle: string, isError: boolean) {
    this.alerts.OneButton(`
    <img src="assets/icon/${isError ? 'error' : 'warning'}.svg" width="125">
    <h3 class="color1">${title}</h3>
    <p>${subTitle}</p>
  `,"Continuar");
  }

  dismiss(): Promise<any> {
    return this.$viewCtrl.dismiss()
  }

  onFileAttachment($event) {
    const { files } = $event.target
    const filteredFiles = this._getCorrectFiles(files)

    if (files.length > filteredFiles.length) {
      this.toast.ShowError('Los archivos duplicados, con un tamaño superior a 5MB o con un formato distinto a PDF/JPG fueron ignorados');
    }

    if ((filteredFiles.length + this.files.length) > this.maxFiles) {
      this.toast.ShowError('Se admiten un máximo de 5 archivos')
    } else {

     
      this.files.push(...filteredFiles)
    }

 
    this.inputFiles.nativeElement.value = ''
  }

  takePhoto () {
 
        this.camera.TakePhoto(uri=>{



        const xhr = new XMLHttpRequest()



        this.loading.Normal('Cargando foto...');

        xhr.open('GET', uri, true)
        xhr.responseType = 'blob'

        xhr.onload = _ => {
          this.createFile(xhr).then(file=>{


          if(file.size>NewRequestPage.FILE_MAX_SIZE){

            this.toast.ShowMessage('Los archivos duplicados, con un tamaño superior a 5MB'+
             'o con un formato distinto a PDF/JPG fueron ignorados');
            }else{
              this.files.push(file)
              this.loading.Hide();
            }

            });
       }

        xhr.send(null);

          

      });
  
  }

  async createFile(xhr:any){
   return await new (window as any)._File([xhr.response], `camera-photo-${this.fileUID++}.jpg`, { type: 'image/jpeg' })
  }

  attachFiles() {
    this.inputFiles.nativeElement.click()
  }

  detachFile(file) {
   
    this.files.splice(this.files.indexOf(file), 1)
  }

  sendAttachments() {
    const user = this.$store.getParse('')
    const company = user.informacionPersonal.empleadores.filter(company => company.activa)[0]

    this.loading.Normal('Enviando solicitud...')

    const request = {
      numeroSolicitud: 1611040001,
      tipoIdentificacionPaciente: 'CC',
      numeroIdentificacionPaciente: user.username,
      tipoIdentificacionEmpleador: 'NI',
      numeroIdentificacionEmpleador: company.numeroDocumento,
      numeroSiniestro: this.data.numeroSiniestro,
      // procedimientos: [], omitted on service
      diagnosticos: []
    }

    this.$consultService.sendAttachments(request, this.files)
      .then(_ => {
        this.loading.Hide()
        return this.dismiss()
      })
      .then(() => {
        this._showAlert('Solicitud exitosa', 'Tu solicitud se encuentra en proceso de gestión. Recuerda que tu respuesta la recibirás a través de un mensaje de texto o consultando la aplicación móvil', false)
      })
 
  }
}
