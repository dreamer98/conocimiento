import { Component } from '@angular/core'
import { NavParams, ViewController } from 'ionic-angular'

@Component({
  selector: 'page-new-info',
  templateUrl: 'new-info.html',
})
export class NewInfoPage {
  data: any

  constructor(
    $navParams: NavParams,
    private $viewCtrl: ViewController) {
    this.data = $navParams.get('data')
    this.data.mainImage="assets/imgs/biciusuario_1.jpg";
  }

  dismiss() {
    this.$viewCtrl.dismiss()
  }
}
