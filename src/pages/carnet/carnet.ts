import { UserDataService } from './../../services/userData.service';
import { AlertsService } from './../../services/alerts.service';
import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { ItemsService } from '../../services/items.service'
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'page-carnet',
  templateUrl: 'carnet.html'

})
export class CarnetPage {
  imgFront:string="assets/imgs/carnet_1.jpg";
  imgBack:string="assets/imgs/carnet_2.jpg";
  infoPersonaCarnet ={};
  infoCarnet:any;
  usuario = {};
  vigencia;
  showCarnet: boolean = false
  datosUsuario : any;
  phonesNumbers = ['', ''];

  relacionLaboral: any = {};

 
  private carnetPromise: any



  constructor(
    public navCtrl: NavController,
    private $itemsService: ItemsService,
    private loading:LoadingService,
    private alerts:AlertsService,
    private userData:UserDataService) {
    
    }

    ionViewDidEnter(){
     /* this.user = this.$store.getParse('user')


      const empleadores = this.user.informacionPersonal.empleadores.filter(empleador => empleador.activa)
*/
 this.carnetPromise = Promise.resolve({ phone_1: '786 838 0755', phone_2: '(571) 743 0106' })
 .then(responds=> {
   this.infoCarnet = responds;

 })

     
     // let relaciones=[];
      if(this.userData.empleadoresActivos.length > 1){
   
    /*  empleadores.map(relacionLaboral => {
          
        relaciones.push(relacionLaboral);
      })
      console.log(relaciones);*/
  
    this.alerts.RelacionLaboral(this.userData.empleadoresActivos,relacion=>{
      this._loadCarnetInfo(relacion);
    });
    }else{
      this._loadCarnetInfo(this.userData.empleadoresActivos[0]);
    }

  }
        
      
    

    _loadCarnetInfo(relacionLaboral) {
      this.relacionLaboral = relacionLaboral
      const razonSocial = relacionLaboral.razonSocial

      this.loading.Normal("Generando carné...");

      this.showCarnet = false

       razonSocial.indexOf('FISCALIA') !== -1 ? 'Fiscalía' :
        razonSocial.indexOf('RAMA JUDICIAL') !== -1 ? 'Rama Judicial' :
        relacionLaboral.topMil ? 'Línea Oro' : 'General'

      let tels


      let relacion ;
    let razon
    for(let i = 0 ; i <this.userData.empleadoresActivos.length ; i++ ){
      
             relacion = this.userData.empleadoresActivos[i]
             razon = relacion.razonSocial.split(" ");
      
     
    }
   

    console.log(razon[0])
    if (razon[0] == "FISCALIA") {

      tels =     "(031) 6000982;018000915809"

    } else if (razon[0] == "RAMA" && razon[1] == "JUDICIAL") {
      tels = "(031) 6000811;01800941541";
    }
      else if (razon[0] == "POSITIVA" || razon == "POSITIVA"){
        tels = "(031) 6000811;01800094154"
      }
      else  if(relacion.marcaEmpresa.topMil){
       razon = "POSITIVA"
        if (razon[0] == "POSITIVA" || razon  =="POSITIVA"){
         tels = "(031) 6000811;01800094154"
       }
     }

     else {
       tels = "(031) 3307000;018000111170";
     }
      

      this.carnetPromise
      .then(() => {
        this.phonesNumbers = tels.split(';');
        return this.$itemsService.datosCarnet();
      })
      .then(response => {
       
        const data = response.data
        let date=new Date(response.fechaActual);
        
        this.vigencia = date.toISOString().split("T")[0];
        console.log(data[0][0])
        this.datosUsuario = data[0][0]
        this.showCarnet = true

        this.loading.Hide(1);
      })
      }
    
   
  }
