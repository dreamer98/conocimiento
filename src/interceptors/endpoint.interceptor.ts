
// Environment configuration
import env from '../env'

// Vendor
import { Injectable } from '@angular/core'
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import { StoreService } from '../services/store.service'

@Injectable()
export class EndpointInterceptor implements HttpInterceptor {

  constructor(
    private $store: StoreService) {}

    static BENEFICIARIO = {
      REGEX: /^@beneficiario/,
      URL: env.BENEFICIARIO_UAT_API
    }

    static CUIDA = {
      REGEX: /^@cuida/,
      URL: env.CUIDA_UAT_API
    }
    static BACKEND ={
      REGEX: /^@backend/,
      URL: env.BACKEND_UAT_API
    }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let { url } = request

    if (EndpointInterceptor.BENEFICIARIO.REGEX.test(url)) {
      url = url.replace(EndpointInterceptor.BENEFICIARIO.REGEX, EndpointInterceptor.BENEFICIARIO.URL + '/backend/public/api/v1')
    } else if (EndpointInterceptor.CUIDA.REGEX.test(url)) {
      url = url.replace(EndpointInterceptor.CUIDA.REGEX, EndpointInterceptor.CUIDA.URL)
    }
    else if (EndpointInterceptor.BACKEND.REGEX.test(url)){
      url = url.replace(EndpointInterceptor.BACKEND.REGEX, EndpointInterceptor.BACKEND.URL +'/positiva/api' )
    }
    else {
      url = /^https?:\/\//.test(url) ? url :  env.API_ROOT + url
    }

    const req: any = { url }
    const token = this.$store.get('user')

    const inBackend = url.includes('beneficiariotesting') || url.includes('beneficiariouatbackend')

    if (token && inBackend) {
        req.headers = new HttpHeaders({
          Authorization:token
    })
  }
    else if (inBackend && url.includes('usuarios')) {
       req.headers = new HttpHeaders({
        'Access-Source':'APP_MOVIL'
       })

        }


    return next.handle(request.clone(req))
  
  }
}
