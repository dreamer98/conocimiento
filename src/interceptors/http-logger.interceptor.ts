import { HomePage } from './../pages/home/home';

import env from '../env'
// Vendor
import { Injectable} from '@angular/core'
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse,HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import 'rxjs/add/operator/timeout';
import { LoggerService } from '../services/logger.service'

import 'rxjs/add/operator/do'
import { LoadingService } from '../services/loading.service';
import { AlertsService } from '../services/alerts.service';
import { LoginPage } from '../pages/login/login';
import { Events } from 'ionic-angular';
import { StoreService } from '../services/store.service';
@Injectable()
export class HttpLoggerInterceptor implements HttpInterceptor {
   defaultTimeout = 15000;

  constructor(
    private $logger: LoggerService,
    private loading:LoadingService,
    private alert: AlertsService,
    private event:Events,
    private $store:StoreService
   ) {

    }
    static BENEFICIARIO = {
      REGEX: /^@beneficiario/,
      URL: env.BENEFICIARIO_UAT_API
    }

    static CUIDA = {
      REGEX: /^@cuida/,
      URL: env.CUIDA_UAT_API
    }
    static BACKEND ={
      REGEX: /^@backend/,
      URL: env.BACKEND_UAT_API
    }



  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let { url } = request

    if (HttpLoggerInterceptor.BENEFICIARIO.REGEX.test(url)) {
      url = url.replace(HttpLoggerInterceptor.BENEFICIARIO.REGEX, HttpLoggerInterceptor.BENEFICIARIO.URL + '/backend/public/api/v1')
    } else if (HttpLoggerInterceptor.CUIDA.REGEX.test(url)) {
      url = url.replace(HttpLoggerInterceptor.CUIDA.REGEX, HttpLoggerInterceptor.CUIDA.URL)
    }
    else if (HttpLoggerInterceptor.BACKEND.REGEX.test(url)){
      url = url.replace(HttpLoggerInterceptor.BACKEND.REGEX, HttpLoggerInterceptor.BACKEND.URL +'/positiva/api' )
    }
    else {
      url = /^https?:\/\//.test(url) ? url :  env.API_ROOT + url
    }

    const req: any = { url }
    const token = this.$store.get('user')

    const inBackend = url.includes('beneficiariotesting') || url.includes('beneficiariouatbackend')

    if (token && inBackend) {
        req.headers = new HttpHeaders({
          Authorization:token,
          'Access-Source':'APP_MOVIL'
    })
  }
    else if (inBackend && url.includes('usuarios')) {
       req.headers = new HttpHeaders({
        'Access-Source':'APP_MOVIL'
       })

        }
    let timeout = Number(request.headers.get('timeout')) || this.defaultTimeout;
    let val1=request.url.indexOf("beneficiario/");
    let val2=request.url.indexOf("/solicitudes");
    let val3=request.url.indexOf("/beneficiarios");
    let val4=request.url.indexOf("/siniestros");
    let val5=request.url.indexOf("/solicitudes");
    if((val1>-1 && request.method=="POST") ||
    (val2>-1 && request.method=="GET") ||
    (val3>-1 && request.method=="POST") ||
    (val4>-1 && request.method=="GET") || 
    (val5>-1 && request.method=="POST")){
      timeout=120000;
    }

    return next.handle(request.clone(req))
      .do(event => {
        if (event instanceof HttpResponse || event instanceof HttpErrorResponse) {
          const url = new URL(event.url)

          this.$logger.group(`${request.method} ${url.pathname}${url.search}:`, log => {
            log('Status:', event.status)
            log('Body:', (event as any).body)
          })
        }
      })
      .timeout(timeout)
      .pipe(
        tap(
          data => {console.log(data);},
         error=>{
           console.error(error);
           this.loading.Hide();
           if(error.error!=undefined){
            if(error.error.error=="Token no válido" || error.error.error=="Token no valido"){
              this.event.publish("setRoot",LoginPage,{showMessage:true});
              this.event.unsubscribe("OpenPage");

            }else if(error.status>=400 || error.httpResponseCode>=400){
              this.alert.OneButton("<img src='assets/icon/warning.svg' width='125'><br>En este momento no se pudo realizar tu solicitud,"+
              " por favor intenta más tarde.","Aceptar",()=>{
                this.loading.Hide();
                this.event.publish("OpenPage",HomePage);
              },false);
            }


        }else if(error.name=="TimeoutError" || error.status>=400 || error.httpResponseCode>=400){

          this.alert.OneButton("<img src='assets/icon/warning.svg' width='125'><br>En este momento no se pudo realizar tu solicitud,"+
          " por favor intenta más tarde.","Aceptar",()=>{
            this.loading.Hide();
            this.event.publish("OpenPage",HomePage);
          },false);

         }
        }
        )
      )
  }



}
